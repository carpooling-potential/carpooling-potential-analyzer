import json
import networkx as nx
import synpp


graph: nx.DiGraph = synpp.Synpp.build_from_yml("config.yml").run_pipeline(dryrun=True)

mapping = {node: node.split("__")[0] for node in graph.nodes}
relabeled = nx.relabel_nodes(graph, mapping)

with open("flowchart.cyjs", "w") as f:
    f.write(json.dumps(nx.cytoscape_data(relabeled)))
