# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
# 
# aina.rasoldier@inria.fr
# 
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""
Matching step.

Matching is the process of assigning trips to carpooling groups such that each
carpooling group has only shareable trips in it and respect other defined
constraints (e.g. each subchain is either passenger or driver and each
passenger trip has a match).
"""
from networkx import DiGraph
from pandas import DataFrame

def get_temp_load(
    graph: DiGraph,
    trips: DataFrame,
    start: int,
    end: int,
    driver: int,
):
    if driver not in graph:
        return 0

    # intersection of intervals of time
    passengers = list(graph.predecessors(driver))
    ptrips_dt = trips.loc[passengers, "departure_time"]
    ptrips_at = trips.loc[passengers, "arrival_time"]
    return (ptrips_dt.lt(end) & ptrips_at.gt(start)).sum()


def get_passenger_count(graph, trips, start, end, driver):
    if driver not in graph:
        return 0

    # NO intersection of intervals of time
    return graph.in_degree(driver)
