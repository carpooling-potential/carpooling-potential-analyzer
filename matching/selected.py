def configure(context):
    context.config("matching.passenger_capacity")
    if context.config("matching.passenger_capacity") > 0:
        if context.config("matching.maximal_matching"):
            context.stage("matching.methods.exact_1p", alias="pools")
        elif context.config("matching.consider_vehicle_location"):
            context.stage("matching.methods.greedy_tours", alias="pools")
        else:
            context.stage("matching.methods.greedy", alias="pools")
    else:
        context.stage("synthesis.metadata")


def execute(context):
    if context.config("matching.passenger_capacity") == 0:
        trips = context.stage("synthesis.metadata")
        context.set_info("shareable_vmt", 0)
        context.set_info("avoided_vmt", 0)
        return trips.index.values.reshape(-1, 1)

    context.set_info(
        "shareable_vmt", context.get_info("pools", "shareable_vmt")
    )
    context.set_info("avoided_vmt", context.get_info("pools", "avoided_vmt"))
    return context.stage("pools")
