# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
# 
# aina.rasoldier@inria.fr
# 
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""
Matching step.

Matching is the process of assigning trips to carpooling groups such that each
carpooling group has only shareable trips in it and respect other defined
constraints (e.g. each subchain is either passenger or driver and each
passenger trip has a match).
"""

import networkx as nx
import pandas as pd
import synpp
import itertools

from commons import catchtime, stars_to_groups, get_shareable_trips_idx
from matching.functions import get_temp_load, get_passenger_count


def configure(context):
    """Configure."""
    context.config("matching.passenger_capacity")
    context.config("matching.variable_load")
    context.stage("synthesis.metadata")
    context.stage("shareability.graph")
    context.stage("matching.tours.filtered")


def get_loads(H, H_temp, trips, start, end, potential_drivers, get_load):
    return {
        potential_driver: get_load(H, trips, start, end, potential_driver)
        + get_load(H_temp, trips, start, end, potential_driver)
        for potential_driver in potential_drivers
    }


def find_candidate(capacity: int, loads: dict, trips_in_tours: set):
    try:
        return next(
            (driver for driver, load in loads.items() if 0 < load < capacity)
        )
    except StopIteration:
        pass

    try:
        return next(
            (
                driver
                for driver, load in loads.items()
                if load == 0 and driver not in trips_in_tours
            )
        )
    except StopIteration:
        return next(
            (driver for driver, load in loads.items() if load == 0), None
        )


def greedy_maximum_star_matching(
    progress: synpp.ProgressContext,
    G: nx.Graph,
    capacity: int,
    trips: pd.DataFrame,
    tours: list,
    variable_load: bool,
):
    get_load = get_temp_load if variable_load else get_passenger_count

    H = nx.DiGraph()
    trips_in_tours = set(itertools.chain(*tours))
    for tour in tours:
        H_temp = nx.DiGraph()
        for u in tour:
            if u not in H.nodes:
                loads = get_loads(
                    H,
                    H_temp,
                    trips,
                    trips.at[u, "departure_time"],
                    trips.at[u, "arrival_time"],
                    G.neighbors(u),
                    get_load,
                )
                v = find_candidate(capacity, loads, trips_in_tours)
                if v is None:
                    break
                H_temp.add_edge(u, v)
            else:
                v = None
                break
        if v is not None:
            H.add_edges_from(H_temp.edges)
            G.remove_nodes_from(tour)
        progress.update()

    return H


def execute(context):
    """Execute."""
    graph = context.stage("shareability.graph").copy()
    tours = context.stage("matching.tours.filtered")
    trips = context.stage("synthesis.metadata")
    capacity = context.config("matching.passenger_capacity")
    variable_load = context.config("matching.variable_load")
    print("Variable load:", variable_load)
    print("Passenger capacity:", capacity)
    print("Number of edges in the graph:", graph.number_of_edges())
    context.set_info(
        "shareable_vmt",
        trips.loc[
            list(get_shareable_trips_idx(graph)), "routed_distance"
        ].sum(),
    )

    if not graph.number_of_nodes():
        context.set_info("avoided_vmt", 0)
        return []

    with catchtime("Removing directly tours with at least 1 unshareable trip"):
        shareable_trips = get_shareable_trips_idx(graph)
        shareable_tours = [
            tour
            for tour in context.progress(
                tours, total=len(tours), label="Selecting shareable tours"
            )
            if set(tour).issubset(shareable_trips)
        ]

    with catchtime("Greedy stars maximal matching with tours"):
        with context.progress(
            total=len(shareable_tours), label="Maximal matching with tours"
        ) as progress:
            stars = greedy_maximum_star_matching(
                progress,
                graph,
                capacity,
                trips,
                shareable_tours,
                variable_load,
            )
    with catchtime("Create groups tuple (driver first) from star family"):
        groups = stars_to_groups(stars)

    print("Number of formed carpooling groups", len(groups))

    passengers = pd.Index(itertools.chain(*[group[1:] for group in groups]))
    context.set_info(
        "avoided_vmt", trips.loc[passengers, "routed_distance"].sum()
    )
    return groups
