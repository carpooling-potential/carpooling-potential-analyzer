# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
#
# aina.rasoldier@inria.fr
#
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""
Matching step.

Matching is the process of assigning trips to carpooling groups such that each
carpooling group has only shareable trips in it and respect other defined
constraints (e.g. each subchain is either passenger or driver and each
passenger trip has a match).

Implementation of
I. B.-A. Hartman et al., « Theory and Practice in Large Carpooling Problems »,
Procedia Computer Science, vol. 32, p. 339‑347, janv. 2014,
doi: 10.1016/j.procs.2014.05.433.

"""
import itertools

import networkx as nx
import pandas as pd
import synpp

from commons import catchtime, stars_to_groups, get_shareable_trips_idx
from matching.functions import get_temp_load, get_passenger_count


def configure(context):
    """Configure."""
    context.config("matching.passenger_capacity")
    context.config("matching.variable_load")
    context.stage("synthesis.metadata")
    context.stage("shareability.graph")


def get_loads(graph, trips, start, end, potential_drivers, get_load):
    return {
        potential_driver: get_load(graph, trips, start, end, potential_driver)
        for potential_driver in potential_drivers
    }


def find_candidate(capacity: int, loads: dict):
    try:
        return next(
            (
                potential_driver
                for potential_driver, load in loads.items()
                if 0 < load < capacity
            )
        )
    except StopIteration:
        return next(
            (
                potential_driver
                for potential_driver, load in loads.items()
                if load == 0
            ),
            None,
        )


def greedy_maximum_star_matching(
    progress: synpp.ProgressClient,
    G: nx.Graph,
    capacity: int,
    trips: pd.DataFrame,
    variable_load: bool,
):
    get_load = get_temp_load if variable_load else get_passenger_count

    H = nx.DiGraph()

    sorted_idx = (
        trips.loc[list(G.nodes), "routed_distance"]
        .sort_values(ascending=False)
        .index
    )

    for u in sorted_idx:
        if u not in H.nodes:
            loads = get_loads(
                H,
                trips,
                trips.at[u, "departure_time"],
                trips.at[u, "arrival_time"],
                G.neighbors(u),
                get_load,
            )
            v = find_candidate(capacity, loads)
            if v is not None:
                H.add_edge(u, v)  # add directed edge u -> v in H
                G.remove_node(u)
        progress.update()

    return H


def execute(context):
    """Execute."""
    graph = context.stage("shareability.graph").copy()
    trips = context.stage("synthesis.metadata")
    context.set_info(
        "shareable_vmt",
        trips.loc[
            list(get_shareable_trips_idx(graph)), "routed_distance"
        ].sum(),
    )

    if not graph.number_of_nodes():
        context.set_info("avoided_vmt", 0)
        return []

    capacity = context.config("matching.passenger_capacity")
    variable_load = context.config("matching.variable_load")
    print("Variable load:", variable_load)

    with catchtime("Greedy stars maximal matching"):
        with context.progress(
            total=graph.number_of_nodes(),
            label="Maximal matching without tours",
        ) as pbar:
            stars = greedy_maximum_star_matching(
                pbar, graph, capacity, trips, variable_load
            )
    with catchtime("Create groups tuple (driver first) from star family"):
        groups = stars_to_groups(stars)

    print("Number of formed carpooling groups", len(groups))

    passengers = pd.Index(itertools.chain(*[group[1:] for group in groups]))
    context.set_info(
        "avoided_vmt", trips.loc[passengers, "routed_distance"].sum()
    )

    return groups
