# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
# 
# aina.rasoldier@inria.fr
# 
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""
Matching step.

Matching is the process of assigning trips to carpooling groups such that each
carpooling group has only shareable trips in it and respect other defined
constraints (e.g. each subchain is either passenger or driver and each
passenger trip has a match).
"""

import itertools
import networkx as nx
import pandas as pd
import numpy as np

import synpp

from commons import catchtime


def configure(context):
    """Configure."""
    context.stage("synthesis.metadata")
    context.stage("shareability.graph", alias="graph")


def worker(context, graph):
    result = nx.max_weight_matching(graph)
    context.progress.update()
    return result


def execute(context: synpp.ExecuteContext):
    """Execute."""
    trips: pd.DataFrame = context.stage("synthesis.metadata")
    graph: nx.Graph = context.stage("graph").to_undirected()

    with catchtime("To Pandas edgelist"):
        edges = nx.to_pandas_edgelist(graph, source="l", target="r")

    with catchtime("Join"):
        edges = edges.join(trips["routed_distance"].rename("ld"), on="l")
        edges = edges.join(trips["routed_distance"].rename("rd"), on="r")

    edges["weight"] = edges[["ld", "rd"]].max(axis=1)
    edges["source"] = np.where(
        edges["ld"] >= edges["rd"], edges["r"], edges["l"]
    )
    edges["target"] = np.where(
        edges["ld"] >= edges["rd"], edges["l"], edges["r"]
    )

    with catchtime("Graph again"):
        graph = nx.from_pandas_edgelist(edges, edge_attr="weight")

    groups = []
    with catchtime("Exact maximal matching (parallel)"):
        n_connected_components = nx.number_connected_components(graph)
        with context.progress(
            label="Exact maximal matching", total=n_connected_components
        ):
            with context.parallel() as p:
                print(n_connected_components, "connected components")
                groups = list(itertools.chain(
                    *p.imap_unordered(
                        worker,
                        map(graph.subgraph, nx.connected_components(graph)),
                    )
                ))

    print("Number of formed carpooling groups", len(groups))

    print("Reordering groups to maximize avoided vkt...")
    edges = pd.DataFrame(groups, columns=["l", "r"])
    with catchtime("Join"):
        edges = edges.join(trips["routed_distance"].rename("ld"), on="l")
        edges = edges.join(trips["routed_distance"].rename("rd"), on="r")
    edges["weight"] = edges[["ld", "rd"]].max(axis=1)
    edges["driver"] = np.where(
        edges["ld"] >= edges["rd"], edges["r"], edges["l"]
    )
    edges["passenger"] = np.where(
        edges["ld"] >= edges["rd"], edges["l"], edges["r"]
    )

    return list(
        edges[["driver", "passenger"]].itertuples(index=False, name=None)
    )
