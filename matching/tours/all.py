# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
# 
# aina.rasoldier@inria.fr
# 
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Detection of the tours in the trips."""

import pandas as pd

import synpp


def configure(context):
    """Configure."""
    context.stage("synthesis.metadata")


def detect_all_tours(context: synpp.ExecuteContext, trips: pd.DataFrame):
    records = []
    with context.progress(
        total=trips["person_id"].nunique(), label="Detecting tours"
    ) as p:
        for person_id, person_trips in trips.groupby("person_id"):
            places = dict()
            person_trips_sorted = person_trips.sort_values("trip_index")
            person_trips_indexes = person_trips_sorted.index.values

            # Origin of the first trip is at index 0
            places[person_trips_sorted.iloc[0]["preceding_geometry"]] = -1
            for trip_index, (label, trip) in enumerate(
                person_trips_sorted.iterrows()
            ):
                if (
                    trip["preceding_geometry"] != trip["following_geometry"]
                    and trip["following_geometry"] in places
                ):
                    tour = person_trips_indexes[
                        places[trip["following_geometry"]] + 1 : trip_index + 1
                    ]
                    length = trips.loc[tour, "routed_distance"].sum()
                    records.append((trip["following_purpose"], tour, length))
                places[trip["following_geometry"]] = trip_index
            p.update()
    return pd.DataFrame.from_records(
        records, columns=["purpose", "tour", "length"]
    )


def execute(context: synpp.ExecuteContext):
    """Execute."""
    trips: pd.DataFrame = context.stage("synthesis.metadata")

    tours = detect_all_tours(context, trips)
    print("Found", tours.shape[0], "tours")

    print("Sort tours by decreasing length...")
    return tours.sort_values("length", ascending=False)
