# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
#
# aina.rasoldier@inria.fr
#
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
import os
import unittest
import pandas as pd
import rtree
import tempfile
import shutil
import joblib

from shareability.forms.inclusive.worker import find_pairs_in_group
from tests.placeholders import progress


class TestPairs(unittest.TestCase):
    def setUp(self):
        # Create a temporary directory
        self.test_dir = tempfile.mkdtemp()

    def tearDown(self):
        # Remove the directory after the test
        shutil.rmtree(self.test_dir)

    def test_inclusive(self):
        trips = pd.read_pickle("tests/data/trips.p")
        trips = trips[trips["route"].notna()].to_crs(2154)

        constraints = {"space_constraint": 1000, "time_constraint": 1000}

        # Create square bounds around origin and destination points.
        bounds_preceding = trips["proj_preceding_geometry"].buffer(
            constraints["space_constraint"], cap_style=3
        )
        bounds_following = trips["proj_following_geometry"].buffer(
            constraints["space_constraint"], cap_style=3
        )

        # Create R-Tree indexes for origin and destination.
        rtree_preceding = bounds_preceding.sindex
        rtree_following = bounds_following.sindex
        pairs = find_pairs_in_group(
            trips,
            constraints,
            progress(total=trips.shape[0]),
            rtree_preceding,
            rtree_following,
            trips.index,
        )
        self.assertEqual(
            joblib.hash(pairs.sort_values(["source", "target"])),
            "b81b805872241f3d794e02cd2e4d6188",
        )
