# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
# 
# aina.rasoldier@inria.fr
# 
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import unittest
import pandas as pd

from shareability.functions import get_space_mask, get_time_mask


class TestFilter(unittest.TestCase):
    def setUp(self) -> None:
        self.pairs = pd.DataFrame(
            [
                (0, 1, 100, 0, -20, -30, True),
                (2, 0, 200, 20, 30, 20, False),
                (1, 0, 300, 200, 10, -5, False),
            ],
            columns=(
                "source",
                "target",
                "origin_distance",
                "destination_distance",
                "departure_time_difference",
                "arrival_time_difference",
                "departure_constrained",
            ),
        )
        return super().setUp()

    def test_space_filter(self):
        mask = get_space_mask(
            self.pairs, {"origin": 100, "destination": 100}
        )

        self.assertListEqual(mask.to_list(), [True, False, False])
        mask = get_space_mask(
            self.pairs, {"origin": 0, "destination": 0}
        )

        self.assertListEqual(mask.to_list(), [False, False, False])

        mask = get_space_mask(
            self.pairs, {"origin": 500, "destination": 500}
        )
        self.assertListEqual(mask.to_list(), [True, True, True])

    def test_time_filter(self):
        mask = get_time_mask(
            self.pairs, {"origin": [-5, 5], "destination": [-5, 5]}
        )

        self.assertListEqual(mask.to_list(), [False, False, True])

        mask = get_time_mask(
            self.pairs, {"origin": [0, 0], "destination": [0, 0]}
        )

        self.assertListEqual(mask.to_list(), [False, False, False])

        mask = get_time_mask(
            self.pairs,
            {"origin": [-500, 500], "destination": [-500, 500]},
        )

        self.assertListEqual(mask.to_list(), [True, True, True])
