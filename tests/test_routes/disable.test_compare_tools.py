# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
# 
# aina.rasoldier@inria.fr
# 
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Check if each group matches the spatio-temporal constraints."""
import unittest
import pickle

from tests.placeholders import progress

from synthesis.routes.apis.pandana.routes import (
    _get_shortest_paths as get_pandana_sp,
    nx_to_pandana,
    process_paths,
)
from synthesis.routes.apis.osmnx.routes import (
    _get_shortest_paths as get_osmnx_sp,
)


class TestCompareTools(unittest.TestCase):
    def test_compare_pandana_osmnx(self):
        with open("tests/data/oxgraph", "rb") as f:
            oxgraph = pickle.load(f)
        with open("tests/test_routes/node_ids", "rb") as f:
            origin_node_ids, dest_node_ids = pickle.load(f)

        pandana_net = nx_to_pandana(oxgraph)
        osmnx_sp = get_osmnx_sp(oxgraph, origin_node_ids, dest_node_ids)
        pandana_sp = list(
            map(
                lambda e: list(e) if len(e) else None,
                get_pandana_sp(pandana_net, origin_node_ids, dest_node_ids),
            )
        )

        self.assertEqual(osmnx_sp.count(None), pandana_sp.count(None))

        osmnx_processed = process_paths(progress, oxgraph, osmnx_sp)
        pandana_processed = process_paths(progress, oxgraph, pandana_sp)

        for dist_osmnx, dist_pandana, dura_osmnx, dura_osmnx in zip(
            osmnx_processed[1],
            pandana_processed[1],
            osmnx_processed[2],
            pandana_processed[2],
        ):
            if dist_osmnx is not None and dist_pandana is not None:
                self.assertEqual(dist_osmnx, dist_pandana)
                self.assertEqual(dura_osmnx, dura_osmnx)
