# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
# 
# aina.rasoldier@inria.fr
# 
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import unittest
import geopandas as gpd
from shapely import Point, Polygon
from synthesis.routes.apis.osmnx.routes import get_routes_wrapper
from synthesis.routes.apis.osmnx.graph import (
    _get_bounding_polygon,
    _get_graph,
)
from tests.placeholders import progress, parallel

from commons import get_geometries


class TestOSMnx(unittest.TestCase):
    def test_get_bounding_polygon(self):
        geometries = gpd.GeoSeries(
            [
                Point(0, 0),
                Point(20, 20),
                Point(0, 5),
                Point(10, 10),
                Point(40, 3),
                Point(3, -4),
                Point(20, 10),
                Point(20, 0),
            ]
        )
        polygon = _get_bounding_polygon(geometries)
        self.assertTrue(
            polygon.contains(
                Polygon(((3, -4), (0, 0), (0, 5), (20, 20), (40, 3), (3, -4)))
            )
        )

    def test_get_routes(self):
        trips = gpd.GeoDataFrame(
            [
                (
                    Point(5.7154, 45.1877),  # 45.1877, 5.7154
                    Point(5.7547, 45.2010),  # 45.2010, 5.7547
                ),
                (
                    Point(5.7333, 45.1892),  # 45.1892, 5.7333
                    Point(5.7528, 45.1847),  # 45.1847, 5.7528
                ),
            ],
            columns=("preceding_geometry", "following_geometry"),
        )
        trips["preceding_geometry"] = gpd.GeoSeries(
            trips["preceding_geometry"]
        )
        trips["following_geometry"] = gpd.GeoSeries(
            trips["following_geometry"]
        )
        geometries = get_geometries(trips)
        polygon = _get_bounding_polygon(geometries)
        graph = _get_graph(polygon)

        routes = get_routes_wrapper(
            progress,
            parallel,
            graph,
            trips["preceding_geometry"],
            trips["following_geometry"],
        )

        self.assertTrue(isinstance(routes, gpd.GeoDataFrame))
        self.assertFalse(routes.empty)
        self.assertFalse(routes.loc[0, "route"].is_empty)
        self.assertFalse(routes.loc[1, "route"].is_empty)
