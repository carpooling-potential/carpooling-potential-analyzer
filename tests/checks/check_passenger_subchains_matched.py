# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
# 
# aina.rasoldier@inria.fr
# 
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Check in each passenger subchain if all trips are matched."""

import itertools

from tests.checks.check_all import print_err, print_header, print_ok


def configure(context):
    """Configure."""
    context.stage("matching.selected", alias="groups")
    context.stage("matching.tours.filtered")


def execute(context):
    """Execute."""
    tours = context.stage("matching.tours.filtered")
    groups = context.stage("groups")

    print_header("All trips of passenger subchains are matched")

    error = False
    passenger_trips = set(itertools.chain(*[group[1:] for group in groups]))
    for passenger_trip in passenger_trips:
        passenger_tour_found = False
        for tour in tours:
            if passenger_trip not in tour:
                continue
            if set(tour).issubset(passenger_trips):
                passenger_tour_found = True
                break
        if not passenger_tour_found:
            print_err(f"{passenger_trip} is not in a passenger tour")
    if not error:
        print_ok()
