# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
# 
# aina.rasoldier@inria.fr
# 
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Check if each group matches the spatio-temporal constraints."""

import networkx as nx
from tests.checks.check_all import print_err, print_header, print_ok


def configure(context):
    """Configure."""
    context.stage("synthesis.metadata")
    context.stage("shareability.merged", alias="graph")


def execute(context):
    """Execute."""
    graph = context.stage("graph")
    departure_constrained = context.stage("synthesis.metadata")[
        "departure_constrained"
    ]
    print_header("Each group is spatio-temporally compatible (identical)")

    edgelist = nx.to_pandas_edgelist(graph)
    edgelist = edgelist.join(
        departure_constrained.rename("departure_constrained_passenger"),
        on="source",
    )
    edgelist = edgelist.join(
        departure_constrained.rename("departure_constrained_driver"),
        on="target",
    )

    bugs = edgelist["departure_constrained_passenger"].ne(
        edgelist["departure_constrained_driver"]
    )

    if bugs.sum():
        print_err("Constraint on departure mismatch")
        print(edgelist.loc[bugs])
    else:
        print_ok()
