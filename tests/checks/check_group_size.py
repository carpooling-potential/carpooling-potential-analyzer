# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
# 
# aina.rasoldier@inria.fr
# 
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Check if each carpooling group matches the vehicle capacity."""


from tests.checks.check_all import print_err, print_header, print_ok


def configure(context):
    """Configure."""
    context.stage("synthesis.metadata")
    context.config("matching.passenger_capacity")
    context.stage("matching.selected", alias="groups")


def max_intersecting_intervals(intervals):
    # Sort the intervals by start time
    intervals.sort(key=lambda x: x[0])

    # Initialize the maximum number of intersecting intervals
    max_intersections = 0

    # Initialize a set to keep track of the end times of the intervals
    # that are currently intersecting
    intersecting_end_times = set()

    for start, end in intervals:
        # Remove all end times that are before the start time of the current interval
        intersecting_end_times = {
            et for et in intersecting_end_times if et > start
        }

        # Add the end time of the current interval to the set
        intersecting_end_times.add(end)

        # Update the maximum number of intersecting intervals if necessary
        max_intersections = max(max_intersections, len(intersecting_end_times))

    return max_intersections


def execute(context):
    """Execute."""
    groups = context.stage("groups")
    trips = context.stage("synthesis.metadata")
    capacity = context.config("matching.passenger_capacity")

    print_header("Each carpooling group has size " + str(capacity))
    error = False
    for group in groups:
        intervals = trips.loc[
            group, ["departure_time", "arrival_time"]
        ].values.tolist()
        number_of_passengers = max_intersecting_intervals(intervals) - 1
        if number_of_passengers > capacity:
            print_err("Error: " + str(group))
            error = True
    if not error:
        print_ok()
