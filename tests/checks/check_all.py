# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
# 
# aina.rasoldier@inria.fr
# 
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""
During this last step, essential properties are checked on carpooling groups.

If one of the verification fail, the algorithms are wrong.
The algorithms here are intentionaly very naive to keep it clear (it should not
change).

You should launch this step in your "config.yml" to launch all the other steps.
"""

from commons import EscapeSequences


def configure(context):
    """Configure."""
    context.stage("tests.checks.check_group_size", ephemeral=True)
    context.stage("tests.checks.check_unicity", ephemeral=True)

    if context.config("shareability_preparation.inclusive.activate"):
        context.stage(
            "tests.checks.check_spatiotemporal_constraints_inclusive",
            ephemeral=True,
        )

    if context.config("shareability_preparation.identical"):
        context.stage(
            "tests.checks.check_spatiotemporal_constraints_identical",
            ephemeral=True,
        )

    if context.config("matching.consider_vehicle_location"):
        context.stage(
            "tests.checks.check_passenger_subchains_matched", ephemeral=True
        )
        context.stage("tests.checks.check_subchains_are_tours", ephemeral=True)
        context.stage("tests.checks.check_unicity_person_subchain", ephemeral=True)

    if context.config(
        "shareability_preparation.separate_departure_constrained"
    ):
        context.stage(
            "tests.checks.check_departure_constrained_matching", ephemeral=True
        )


def print_header(text):
    """Print title of tests."""
    print(
        ">>>> Test: "
        + EscapeSequences.LIGHTMAGENTA
        + text
        + EscapeSequences.ENDC
    )


def print_ok():
    """When everything's alright."""
    print(
        ">>>> "
        + EscapeSequences.LIGHTGREEN
        + "OK!"
        + EscapeSequences.ENDC
        + " <<<<"
    )


def print_err(text):
    """Wrong result."""
    print(">>>> " + EscapeSequences.LIGHTRED + text + EscapeSequences.ENDC)


def execute(context):
    """Execute."""
    pass
