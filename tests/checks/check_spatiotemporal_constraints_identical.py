# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
# 
# aina.rasoldier@inria.fr
# 
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Check if each group matches the spatio-temporal constraints."""
import synpp

from commons import get_dest, get_orig, time_difference
from tests.checks.check_all import print_err, print_header, print_ok


def configure(context: synpp.ConfigurationContext):
    """Configure."""
    context.stage("synthesis.metadata", alias="trips")
    context.stage("shareability.merged", alias="pairs")
    context.stage("shareability.constraints_mask", alias="mask")
    context.config("shareability.identical.filter")


def execute(context: synpp.ExecuteContext):
    """Execute."""
    trips = context.stage("trips")
    pairs = context.stage("pairs")
    mask = context.stage("mask")
    constraints = context.config("shareability.identical.filter")
    space_constraint = constraints["space_constraint"]
    time_constraint = constraints["time_constraint"]

    filtered_pairs = pairs.loc[mask & pairs["form"].eq("identical")]

    print_header("Each group is spatio-temporally compatible (identical)")
    if filtered_pairs.empty:
        print_err("No identical carpooling pair...")
        return
    error = False
    for pair in filtered_pairs.itertuples():
        driver_id = pair.target
        passenger_id = pair.source
        driver = trips.loc[driver_id]
        passenger = trips.loc[passenger_id]
        distance_origin = get_orig(driver).distance(get_orig(passenger))
        distance_destination = get_dest(driver).distance(
            get_dest(passenger)
        )

        if distance_origin > space_constraint["origin"]:
            print_err(
                f"{passenger_id} -> {driver_id} Distance origin not OK"
            )
            error = True
        if distance_destination > space_constraint["destination"]:
            print_err(
                f"{passenger_id} -> {driver_id} Distance destination not OK"
            )
            error = True

        departure_time_match = (
            time_constraint["origin"][0]
            <= time_difference(
                driver["departure_time"], passenger["departure_time"]
            )
            <= time_constraint["origin"][1]
        )
        arrival_time_match = (
            time_constraint["destination"][0]
            <= time_difference(
                driver["arrival_time"], passenger["arrival_time"]
            )
            <= time_constraint["destination"][1]
        )
        if not (departure_time_match or arrival_time_match):
            print_err(
                f"{passenger_id} -> {driver_id} Time differences not OK "
            )
            error = True
    if not error:
        print_ok()
