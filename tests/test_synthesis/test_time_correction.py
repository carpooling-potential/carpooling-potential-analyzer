# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
# 
# aina.rasoldier@inria.fr
# 
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import unittest
import pandas as pd
from synthesis.routed import departure_constrained_mask, correct_time


class TestTimeCorrection(unittest.TestCase):
    def test_correct_time(self):
        purposes = [
            ("work", "home"),
            ("education", "home"),
        ]
        trips = pd.DataFrame(
            [
                (0, 100, 50, "work", "home"),
                (200, 400, 400, "work", "home"),
                (500, 600, 220, "work", "home"),
                (-400, 100, 50, "education", "home"),
                (0, 100, 50, "home", "work"),
                (200, 400, 400, "home", "work"),
                (500, 600, 220, "home", "work"),
                (-400, 100, 50, "home", "work"),
                (0, 100, None, "home", "work"),
                (200, 400, None, "home", "work"),
                (500, 600, None, "home", "work"),
                (-400, 100, None, "home", "work"),
            ],
            columns=[
                "departure_time",
                "arrival_time",
                "duration_computed",
                "preceding_purpose",
                "following_purpose",
            ],
        )
        trips["departure_constrained"] = departure_constrained_mask(
            trips, purposes
        )
        result = correct_time(trips)
        self.assertListEqual(
            result["departure_constrained"].tolist(),
            [
                True,
                True,
                True,
                True,
                False,
                False,
                False,
                False,
                False,
                False,
                False,
                False,
            ],
        )
        self.assertListEqual(
            result["departure_time"].tolist(),
            [0, 200, 500, -400, 50, 0, 380, 50, 0, 200, 500, -400],
        )
