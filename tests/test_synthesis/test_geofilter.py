# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
# 
# aina.rasoldier@inria.fr
# 
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

# TODO: time correction
import unittest
import geopandas as gpd
from shapely import Point, Polygon
from synthesis.geofiltered import get_geomask

from tests.placeholders import progress


class TestGeoFilter(unittest.TestCase):
    def test_geo_filter_all_od_in_zone(self):
        trips = gpd.GeoDataFrame(
            [
                (0, Point(0, 2), Point(3, 2), "home", "work", None),
                (1, Point(-1, 2), Point(3, 2), "home", "work", None),
                (2, Point(2, 2), Point(-1, 2), "home", "work", None),
                (3, Point(-1, 2), Point(-1, 2), "home", "work", None),
                (4, Point(2, 2), Point(3, 2), "other", "work", None),
                (4, Point(2, 3), Point(5, 10), "home", "work", None),
            ],
            columns=(
                "person_id",
                "proj_preceding_geometry",
                "proj_following_geometry",
                "preceding_purpose",
                "following_purpose",
                "route",
            ),
        )
        zones = gpd.GeoSeries(
            (Polygon(((0, 0), (0, 10), (10, 10), (10, 0), (0, 0))))
        )
        mask = get_geomask(trips, zones, progress)
        with self.subTest():
            self.assertEqual(mask.iloc[0], True)
        with self.subTest():
            self.assertEqual(mask.iloc[1], False, msg="O is outside")
        with self.subTest():
            self.assertEqual(mask.iloc[2], False, msg="D is outside")
        with self.subTest():
            self.assertEqual(mask.iloc[3], False, msg="OD are outside")
        with self.subTest():
            self.assertEqual(mask.iloc[4], True)
        with self.subTest():
            self.assertEqual(mask.iloc[5], True)
