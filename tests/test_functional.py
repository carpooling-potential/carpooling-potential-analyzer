# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
# 
# aina.rasoldier@inria.fr
# 
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Check if each group matches the spatio-temporal constraints."""

import tempfile
import shutil
import unittest
import itertools
import synpp
import yaml
import numpy as np

from commons import get_orig, get_dest, time_difference


def max_intersecting_intervals(intervals):
    # Sort the intervals by start time
    intervals.sort(key=lambda x: x[0])

    # Initialize the maximum number of intersecting intervals
    max_intersections = 0

    # Initialize a set to keep track of the end times of the intervals
    # that are currently intersecting
    intersecting_end_times = set()

    for start, end in intervals:
        # Remove all end times that are before the start time of the current interval
        intersecting_end_times = {
            et for et in intersecting_end_times if et > start
        }

        # Add the end time of the current interval to the set
        intersecting_end_times.add(end)

        # Update the maximum number of intersecting intervals if necessary
        max_intersections = max(max_intersections, len(intersecting_end_times))

    return max_intersections


class TestFunctional(unittest.TestCase):
    @classmethod
    def tearDownClass(cls):
        # Remove the directory after the test
        shutil.rmtree(cls.test_dir)

    @classmethod
    def setUpClass(cls) -> None:
        # Create a temporary directory
        cls.test_dir = tempfile.mkdtemp()

        with open("tests/data/config_functional.yml") as f:
            cls.config = yaml.load(f, yaml.Loader)

        (
            cls.tours,
            cls.trips,
            cls.groups,
            cls.groups_tours,
        ) = synpp.run(
            [
                {"descriptor": "matching.tours.filtered"},
                {"descriptor": "synthesis.routed"},
                {"descriptor": "matching.methods.greedy"},
                {"descriptor": "matching.methods.greedy_tours"},
            ],
            cls.config,
            working_directory=cls.test_dir,
        )
        return super().setUpClass()

    def test_unicity(self):
        carpooled_trips = sum(self.groups, ())
        self.assertEqual(len(carpooled_trips), len(set(carpooled_trips)))

    def test_unicity_tours(self):
        carpooled_trips = sum(self.groups_tours, ())
        self.assertEqual(len(carpooled_trips), len(set(carpooled_trips)))

    def test_unicity_person_subchain(self):
        for tour_set in self.tours:
            self.assertEqual(
                self.trips.loc[tour_set, "person_id"].nunique(), 1
            )

    def test_subchains_are_tours(self):
        for tour_set in self.tours:
            first_trip_origin = get_orig(self.trips.loc[tour_set[0]])
            last_trip_destination = get_dest(self.trips.loc[tour_set[-1]])
            self.assertEqual(first_trip_origin, last_trip_destination)

    def test_passenger_subchains_matched(self):
        passenger_trips = set(
            itertools.chain(*[group[1:] for group in self.groups_tours])
        )
        for passenger_trip in passenger_trips:
            passenger_tour_found = False
            for tour in self.tours:
                if passenger_trip not in tour:
                    continue
                if set(tour).issubset(passenger_trips):
                    passenger_tour_found = True
                    break
            self.assertTrue(passenger_tour_found)

    def test_group_size(self):
        """Execute."""
        for capacity in range(1, 5):
            with self.subTest(capacity=capacity):
                groups = synpp.run(
                    [
                        {
                            "descriptor": "matching.methods.greedy",
                            "config": {
                                "matching": {"passenger_capacity": capacity}
                            },
                        },
                    ],
                    self.config,
                    working_directory=self.test_dir,
                )[0]
                for group in groups:
                    intervals = self.trips.loc[
                        group, ["departure_time", "arrival_time"]
                    ].values.tolist()
                    number_of_passengers = (
                        max_intersecting_intervals(intervals) - 1
                    )
                    self.assertLessEqual(number_of_passengers, capacity)

    def test_group_size_tours(self):
        """Execute."""
        for capacity in range(1, 5):
            with self.subTest(capacity=capacity):
                groups = synpp.run(
                    [
                        {
                            "descriptor": "matching.methods.greedy_tours",
                            "config": {
                                "matching": {"passenger_capacity": capacity}
                            },
                        },
                    ],
                    self.config,
                    working_directory=self.test_dir,
                )[0]
                for group in groups:
                    intervals = self.trips.loc[
                        group, ["departure_time", "arrival_time"]
                    ].values.tolist()
                    number_of_passengers = (
                        max_intersecting_intervals(intervals) - 1
                    )
                    self.assertLessEqual(number_of_passengers, capacity)

    def test_identical_graph_spatiotemporal_constraints(self):
        space_constraint = self.config["shareability_preparation"][
            "identical"
        ]["max"]["space_constraint"]
        time_constraint = self.config["shareability_preparation"]["identical"][
            "max"
        ]["time_constraint"]

        pairs = synpp.run(
            [{"descriptor": "shareability.forms.identical.pairs"}],
            self.config,
            working_directory=self.test_dir,
        )[0]

        for pair in pairs.itertuples():
            passenger = self.trips.loc[pair.source]
            driver = self.trips.loc[pair.target]

            distance_origin = get_orig(driver).distance(get_orig(passenger))
            self.assertLessEqual(distance_origin, space_constraint)

            distance_destination = get_dest(driver).distance(
                get_dest(passenger)
            )
            self.assertLessEqual(distance_destination, space_constraint)

            departure_time_difference = abs(
                time_difference(
                    driver["departure_time"], passenger["departure_time"]
                )
            )
            self.assertLessEqual(departure_time_difference, time_constraint)

            arrival_time_difference = abs(
                time_difference(
                    driver["arrival_time"], passenger["arrival_time"]
                )
            )
            self.assertLessEqual(arrival_time_difference, time_constraint)

class TestInclusiveGraph(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        # Create a temporary directory
        cls.test_dir = tempfile.mkdtemp()

    @classmethod
    def tearDownClass(cls):
        # Remove the directory after the test
        shutil.rmtree(cls.test_dir)

    def test_inclusive_graph_spatiotemporal_constraints(self):
        """Execute."""
        with open("tests/data/config_functional.yml") as f:
            config = yaml.load(f, yaml.Loader)

        trips, pairs, mask = synpp.run(
            [
                {"descriptor": "synthesis.routed"},
                {"descriptor": "shareability.forms.inclusive.raw_pairs"},
                {"descriptor": "shareability.forms.inclusive.mask"},
            ],
            config,
            working_directory=self.test_dir,
        )

        constraints = config["shareability"]["inclusive"]["filter"]
        space_constraint = constraints["space_constraint"]
        time_constraint = constraints["time_constraint"]

        # Re-projecting to projected CRS for the distances calculation
        trips["route"] = trips["route"].to_crs(epsg=2154)

        self.assertFalse(pairs.empty, "No pairs")
        for pair in pairs.loc[mask].itertuples():
            passenger_id = pair.source
            driver_id = pair.target
            driver = trips.loc[driver_id]
            passenger = trips.loc[passenger_id]

            passenger_orig = passenger["proj_preceding_geometry"]
            passenger_dest = passenger["proj_following_geometry"]
            distance_orig = passenger_orig.distance(driver["route"])
            distance_dest = passenger_dest.distance(driver["route"])

            self.assertLessEqual(
                distance_orig,
                space_constraint["origin"],
                f"{passenger_id} -> {driver_id} Distance origin not OK",
            )
            self.assertLessEqual(
                distance_dest,
                space_constraint["destination"],
                f"{passenger_id} -> {driver_id} Distance destination not OK",
            )

            projs = driver["route"].project([passenger_orig, passenger_dest])

            passages = np.interp(
                projs, driver["timing"][:, 0], driver["timing"][:, 1]
            )

            dep_passage, arr_passage = driver["departure_time"] + passages

            dep_match = (
                time_constraint["origin"][0]
                <= time_difference(dep_passage, passenger["departure_time"])
                <= time_constraint["origin"][1]
            )

            arr_time_match = (
                time_constraint["destination"][0]
                <= time_difference(arr_passage, passenger["arrival_time"])
                <= time_constraint["destination"][1]
            )

            self.assertLessEqual(
                dep_passage,
                arr_passage,
                f"{passenger_id} and {driver_id} are in opposite directions.",
            )
            self.assertTrue(
                dep_match or arr_time_match,
                f"{passenger_id} -> {driver_id} Time differences not OK.",
            )
            # self.assertGreaterEqual(
            #     driver["routed_distance"],
            #     passenger["routed_distance"],
            #     f"{passenger_id} -> {driver_id} Driver route is shorter.",
            # )
