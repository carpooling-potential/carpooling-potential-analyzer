# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
# 
# aina.rasoldier@inria.fr
# 
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import unittest
from matching.methods.greedy import find_candidate
from matching.methods.greedy_tours import find_candidate as find_candidate_tour


class TestFindCandidate(unittest.TestCase):
    def test_find_candidate(self):
        loads = {0: 2, 1: 4, 2: 0}
        self.assertEqual(find_candidate(4, loads), 0)
        self.assertEqual(find_candidate(2, loads), 2)

        # No priority between the number of passengers, taking the first one.
        self.assertEqual(find_candidate(5, loads), 0)

        loads = {0: 4, 1: 4, 2: 3, 3: 3}
        self.assertEqual(find_candidate(4, loads), 2)
        self.assertIsNone(find_candidate(3, loads))

        loads = {0: 4, 1: 4, 2: 4}
        # No candidate found because all full
        self.assertIsNone(find_candidate(4, loads))

        loads = dict()
        # No candidate found because no candidate
        self.assertIsNone(find_candidate(4, loads))

    def test_find_candidate_tour(self):
        in_tour = {0, 1, 2}
        loads = {0: 2, 1: 4, 2: 0, 3: 0, 4: 0}
        self.assertEqual(find_candidate_tour(4, loads, in_tour), 0)

        # When several without passenger, take the first not in tour
        self.assertEqual(find_candidate_tour(2, loads, in_tour), 3)

        # No priority between the number of passengers, taking the first one.
        self.assertEqual(find_candidate_tour(5, loads, in_tour), 0)

        loads = {0: 4, 1: 4, 2: 3, 3: 3}
        self.assertEqual(find_candidate_tour(4, loads, in_tour), 2)
        self.assertIsNone(find_candidate_tour(3, loads, in_tour))

        loads = {0: 4, 1: 4, 2: 4}
        # No candidate found because all full
        self.assertIsNone(find_candidate_tour(4, loads, in_tour))

        loads = dict()
        # No candidate found because no candidate
        self.assertIsNone(find_candidate_tour(4, loads, in_tour))
