# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
# 
# aina.rasoldier@inria.fr
# 
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import unittest
import networkx as nx
import pandas as pd
from matching.functions import get_temp_load, get_passenger_count


class TestLoads(unittest.TestCase):
    def setUp(self) -> None:
        """
          0----0----1----1----2----2----3----3----4----4----5----5----6----6
          0    5    0    5    0    5    0    5    0    5    0    5    0    5
        0           <-------->
        1                <-------->
        2                               <-------->
        3                                                   <--------->
        4           <-------------------------------------->

        4 is driver of 0, 1, and 2
        3 is driving alone
        """
        self.trips = pd.DataFrame(
            [
                [10, 20],
                [15, 25],
                [30, 40],
                [50, 60],
                [10, 50],
            ],
            index=[0, 1, 2, 3, 4],
            columns=["departure_time", "arrival_time"],
        )
        self.graph = nx.DiGraph()
        self.graph.add_edges_from([(0, 4), (1, 4), (2, 4)])
        return super().setUp()

    def test_temp_load(self):
        self.assertEqual(get_temp_load(self.graph, self.trips, 0, 20, 4), 2)
        self.assertEqual(get_temp_load(self.graph, self.trips, 0, 40, 4), 3)
        self.assertEqual(get_temp_load(self.graph, self.trips, 20, 25, 4), 1)

        # Checking a capacity outside the driver's trip is OK
        self.assertEqual(get_temp_load(self.graph, self.trips, 0, 40, 3), 0)

        self.assertEqual(get_temp_load(self.graph, self.trips, 50, 60, 3), 0)

    def test_constant_load(self):
        self.assertEqual(
            get_passenger_count(self.graph, self.trips, 0, 20, 4), 3
        )
        self.assertEqual(
            get_passenger_count(self.graph, self.trips, 0, 40, 4), 3
        )
        self.assertEqual(
            get_passenger_count(self.graph, self.trips, 20, 25, 4), 3
        )

        # Checking a capacity outside the driver's trip is OK
        self.assertEqual(
            get_passenger_count(self.graph, self.trips, 0, 40, 3), 0
        )

        self.assertEqual(
            get_passenger_count(self.graph, self.trips, 50, 60, 3), 0
        )
