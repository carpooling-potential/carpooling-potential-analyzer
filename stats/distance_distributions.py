# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
# 
# aina.rasoldier@inria.fr
# 
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Show distribution of distances routed, euclidian and estimated."""
import synpp
import pandas as pd
import geopandas as gpd
from matplotlib import pyplot as plt

STEP = .1

def configure(context):
    """Configure."""
    context.stage("synthesis.geofiltered")


def plot_cdf(series: pd.Series, ax: plt.Axes, label: str):
    cumsum = series.value_counts(sort=False).sort_index().cumsum()
    return ax.plot(cumsum.index, cumsum.values, label=label)


def execute(context: synpp.ExecuteContext):
    """Execute."""
    # Get trips with routes
    trips: gpd.GeoDataFrame = context.stage("synthesis.geofiltered")

    fig, ax = plt.subplots()
    plot_cdf(trips["routed_distance"] / 1000, ax, label="Routed")
    plot_cdf(trips["euclidean_distance"] / 1000, ax, label="Euclidean")
    plot_cdf(trips["euclidean_distance"] * 1.3 / 1000, ax, label="Euclidan×1.3")
    ax.set_xlabel("Distance (km)")
    ax.set_ylabel("Trips count")
    ax.set_xlim(left=0)
    ax.set_ylim(bottom=0)
    ax.grid()
    fig.legend(loc="right")
    ax.set_title("Cumulative density of routed, euclidean and euclidean×1.3 distances\non synthetic population")
    plt.show()
    return
