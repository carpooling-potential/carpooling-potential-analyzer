# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
#
# aina.rasoldier@inria.fr
#
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
"""Batch execution of matching stats."""
import os

from matplotlib import pyplot as plt
from shapely.geometry import LineString
import numpy as np

import synpp

from commons import get_run_output_path

import stats.metrics
import synthesis.geofiltered

import filtering.carpoolable



def extract_values(data, parameter_name, scenario_names):
    x = []
    y = {}

    for k, v in data.items():
        x.append(float(k))
        for scenario in scenario_names:
            if scenario not in y:
                y[scenario] = []
            y[scenario].append(float(v[scenario]) / 1000)

    if parameter_name == "stats.batch.rate":
        x = [0] + [i * 100 for i in x]
        for scenario in y:
            y[scenario] = [0] + y[scenario]
    return x, y


def plot_parameter(ax: plt.Axes, x, y):
    for scenario in y:
        if scenario.endswith("_notour"):
            linestyle = "dashed"
        else:
            linestyle = "solid"
        ax.plot(
            x,
            y[scenario],
            label=SCENARIO_NAMES[scenario],
            marker=".",
            linestyle=linestyle,
        )


def configure(context: synpp.ConfigurationContext):
    """Configure."""
    context.stage("stats.batch.rate")

    context.config("paths.output")
    context.stage(stats.metrics, alias="stats")
    context.stage(synthesis.geofiltered)
    context.stage(filtering.carpoolable)


PARAMETER_META = {
    "stats.batch.rate": {
        "name": "adoption rate",
        "xlabel": "adoption rate (\%)",
    },
}

SCENARIO_NAMES = {
    # "all_d2dpudo": "All trips, identical and inclusive carpooling",
    "commute_d2dpudo": "Commute trips, identical and inclusive carpooling",
}


def execute(context: synpp.ExecuteContext):
    """Execute."""
    total_commute_vkt = (
        context.get_info(
            filtering.carpoolable,
            "passenger_carpoolable_vmt",
        )
        / 1000
    )

    output_path = get_run_output_path(context)

    fig, ax = plt.subplots(1, 1, figsize=(3.5, 3))
    ax.patch.set_facecolor(None)

    stage = "stats.batch.rate"
    # Set color
    default_color_cycle = plt.rcParams["axes.prop_cycle"].by_key()["color"]
    ax.set_prop_cycle(color=[default_color_cycle[4]])

    data = context.stage(stage)
    x, y = extract_values(data, stage, SCENARIO_NAMES)
    plot_parameter(ax, x, y)

    ax.set_ylabel("Avoided VKT (km)")
    ax.set_xlabel(PARAMETER_META[stage]["xlabel"])
    ax.set_title(
        f'Variation of {PARAMETER_META[stage]["name"]}',
    )

    ax.set_ylim(bottom=0)
    ax.set_xlim(left=0)
    _ymin, ymax = ax.get_ylim()
    ax2 = ax.twinx()
    ax2.patch.set_facecolor(None)
    # ax2.set_ylim(bottom=0, top=ymax * 100 / total_vkt)
    ylims = ax2.set_ylim(bottom=0, top=ymax * 100 / total_commute_vkt)
    ax2.set_ylabel("Avoided VKT as a share of car VKT\nfor commuting (\%)")
    ax.set_zorder(3)
    ax2.set_zorder(0)
    objective = 24
    objective_color = "#93032e"

    x_hori, y_hori = [0, 100], [total_commute_vkt * objective / 100] * 2
    horizontal_line = LineString(np.column_stack((x_hori, y_hori)))
    vkts_line = LineString(np.column_stack((x, y["commute_d2dpudo"])))
    x_inter, _ = horizontal_line.intersection(vkts_line).coords[0]

    yticks = list(filter(lambda x: x <= ylims[1], ax2.get_yticks()))
    ax2.set_yticks(yticks + [objective])
    for label in ax2.get_yticklabels():
        if int(label.get_text()) == objective:
            label.set_color(objective_color)

    xticks = list(map(int, filter(lambda x: x <= 100, ax.get_xticks())))
    ax.set_xticks(xticks + [x_inter], labels=map(str, xticks + [round(x_inter)]))
    for label in ax.get_xticklabels():
        if int(label.get_text()) == round(x_inter):
            label.set_color(objective_color)

    ax2.plot(
        [x_inter, x_inter, 105],
        [0, objective, objective],
        linestyle="dotted",
        color=objective_color,
    )

    # Legends for scenarios
    handles, labels = ax.get_legend_handles_labels()
    fig.legend(
        handles,
        labels,
        loc="upper center",
        bbox_to_anchor=(0.5, -0.1),
        ncols=1,
    )
    fig.subplots_adjust(
        left=0, bottom=0.1, right=1, top=1, wspace=0.3, hspace=0.3
    )

    # Save to file.
    fig.savefig(
        os.path.join(output_path, f"sampling_rate.pgf"),
        bbox_inches="tight",
        transparent=True,
    )
    fig.savefig(
        os.path.join(output_path, f"sampling_rate.pdf"),
        bbox_inches="tight",
        transparent=True,
    )

    plt.close("all")
