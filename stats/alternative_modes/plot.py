# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
# 
# aina.rasoldier@inria.fr
# 
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import os
from matplotlib import pyplot as plt
from matplotlib import ticker

from commons import get_run_output_path

def configure(context):
    context.config("paths.output")
    context.stage("stats.alternative_modes.data")


def execute(context):
    (
        carpool_avoided_vkt,
        pt_time_ratio_cumsum,
        bike_time_ratio_cumsum,
        pt_time_diff_cumsum,
        bike_time_diff_cumsum,
    ) = context.stage("stats.alternative_modes.data")


    # PUBLIC TRANSPORT
    fig_ratio1, ax_ratio1 = plt.subplots()
    ax_ratio1.set_title(
        "Comparison of carpooling and public transport.\n"
        + f"Carpooling can avoid {round(carpool_avoided_vkt / 1000):,} km"
    )
    ax_ratio1.plot(
        pt_time_ratio_cumsum.index,
        pt_time_ratio_cumsum.values / carpool_avoided_vkt * 100,
        label="all trips",
    )

    ax_ratio1.yaxis.set_major_formatter(
        ticker.StrMethodFormatter("{x:,.0f}")
    )
    ax_ratio1.set_ylabel("Share of avoided VKT by carpooling (%)")
    ax_ratio1.set_xlabel("Time factor between car and public transport")
    ax_ratio1.legend()
    ax_ratio1.grid()
    ax_ratio1.set_xlim(left=1)

    # BIKE
    fig_ratio2, ax_ratio2 = plt.subplots()
    ax_ratio2.set_title(
        "Comparison of carpooling and bike.\n"
        + f"Carpooling can avoid {round(carpool_avoided_vkt / 1000):,} km"
    )
    ax_ratio2.plot(
        bike_time_ratio_cumsum.index,
        bike_time_ratio_cumsum.values / carpool_avoided_vkt * 100,
        label="all trips",
    )

    ax_ratio2.yaxis.set_major_formatter(
        ticker.StrMethodFormatter("{x:,.0f}")
    )
    ax_ratio2.set_ylabel("Share of avoided VKT by carpooling (%)")
    ax_ratio2.set_xlabel("Time factor between car and bike")
    ax_ratio2.legend()
    ax_ratio2.grid()
    ax_ratio2.set_xlim(left=1)

    # PUBLIC TRANSPORT
    fig_diff1, ax_diff1 = plt.subplots()
    ax_diff1.set_title(
        "Comparison of carpooling and public transport.\n"
        + f"Carpooling can avoid {round(carpool_avoided_vkt / 1000):,} km"
    )
    ax_diff1.plot(
        pt_time_diff_cumsum.index / 60,
        pt_time_diff_cumsum.values / carpool_avoided_vkt * 100,
        label="public transport",
    )

    ax_diff1.yaxis.set_major_formatter(
        ticker.StrMethodFormatter("{x:,.0f}")
    )
    ax_diff1.set_ylabel("Share of avoided VKT by carpooling (%)")
    ax_diff1.set_xlabel(
        "Difference between car trip and public transport trip durations (min)"
    )

    # BIKE
    ax_diff1.plot(
        bike_time_diff_cumsum.index / 60,
        bike_time_diff_cumsum.values / carpool_avoided_vkt * 100,
        label="bike",
    )

    ax_diff1.yaxis.set_major_formatter(
        ticker.StrMethodFormatter("{x:,.0f}")
    )
    ax_diff1.set_ylabel("Share of avoided VKT by carpooling (%)")
    ax_diff1.set_xlabel(
        "Difference between car trip and bike trip durations (min)"
    )
    ax_diff1.legend()
    ax_diff1.grid()
    ax_diff1.set_xlim(left=0,right=60)
    ax_diff1.set_ylim(bottom=0, top=100)

    output_path = get_run_output_path(context)
    fig_diff1.savefig(os.path.join(output_path, "alternatives_absolute1.pdf"))
    # fig_diff2.savefig(os.path.join(output_path, "alternatives_absolute2.pdf"))
    fig_ratio1.savefig(os.path.join(output_path, "alternatives_relative1.pdf"))
    fig_ratio2.savefig(os.path.join(output_path, "alternatives_relative2.pdf"))
    fig_diff1.savefig(os.path.join(output_path, "alternatives_absolute1.pgf"))
    # fig_diff2.savefig(os.path.join(output_path, "alternatives_absolute2.pgf"))
    fig_ratio1.savefig(os.path.join(output_path, "alternatives_relative1.pgf"))
    fig_ratio2.savefig(os.path.join(output_path, "alternatives_relative2.pgf"))
