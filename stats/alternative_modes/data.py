# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
# 
# aina.rasoldier@inria.fr
# 
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import itertools

import pandas as pd


def configure(context):
    context.stage("synthesis.cleaned", alias="trips")
    context.stage("synthesis.geofiltered")
    context.stage("matching.selected", alias="groups")
    context.stage("routes.pt")
    context.stage("routes.bike")


def execute(context):
    trips = context.stage("trips")
    car_trips_mask = trips["mode"].eq("car")

    car = context.stage("synthesis.geofiltered").loc[car_trips_mask]
    pt = context.stage("routes.pt").loc[car_trips_mask]
    bike = context.stage("routes.bike").loc[car_trips_mask]

    stats = pd.DataFrame()
    stats["avoided_vkt"] = car["routed_distance"]
    stats["car_time"] = car["duration_computed"]

    stats["can_bike"] = True
    stats["bike_time_ratio"] = bike["duration_computed"] / stats["car_time"]
    stats["bike_time_diff"] = bike["duration_computed"] - stats["car_time"]

    stats["can_pt"] = pt["transfers"].ne(-1)
    stats.loc[stats["can_pt"], "pt_time_ratio"] = (
        pt["duration_computed"] / stats["car_time"]
    )
    stats.loc[stats["can_pt"], "pt_time_diff"] = (
        pt["duration_computed"] - stats["car_time"]
    )
    groups = context.stage("groups")

    stats["can_carpool"] = car.index.isin(sum(groups, ()))
    passengers = list(itertools.chain(*[group[1:] for group in groups]))
    stats["is_passenger"] = car.index.isin(passengers)

    carpool_avoided_vkt = car.loc[
        stats["is_passenger"], "routed_distance"
    ].sum()

    # PUBLIC TRANSPORT
    pt_time_ratio_cumsum = (
        stats.loc[stats["is_passenger"]]
        .groupby("pt_time_ratio")["avoided_vkt"]
        .sum()
        .sort_index()
        .cumsum()
        .dropna()
    )

    # BIKE
    bike_time_ratio_cumsum = (
        stats.loc[stats["is_passenger"]]
        .groupby("bike_time_ratio")["avoided_vkt"]
        .sum()
        .sort_index()
        .cumsum()
        .dropna()
    )

    # PUBLIC TRANSPORT
    pt_time_diff_cumsum = (
        stats.loc[stats["is_passenger"]]
        .groupby("pt_time_diff")["avoided_vkt"]
        .sum()
        .sort_index()
        .cumsum()
        .dropna()
    )

    # BIKE
    bike_time_diff_cumsum = (
        stats.loc[stats["is_passenger"]]
        .groupby("bike_time_diff")["avoided_vkt"]
        .sum()
        .sort_index()
        .cumsum()
        .dropna()
    )

    return (
        carpool_avoided_vkt,
        pt_time_ratio_cumsum,
        bike_time_ratio_cumsum,
        pt_time_diff_cumsum,
        bike_time_diff_cumsum,
    )
