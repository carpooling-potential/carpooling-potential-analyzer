# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
# 
# aina.rasoldier@inria.fr
# 
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Stage to print statistics about carpooling potential."""

import synpp
import itertools

import pandas as pd

from commons import get_shareable_trips_idx


def configure(context):
    """Configure."""
    context.config("paths.output")
    context.config("subsampling.rate")
    context.config("matching.passenger_capacity")
    context.stage("synthesis.metadata")
    context.stage("synthesis.geofiltered")
    context.stage("shareability.graph", alias="graph")
    if context.config("matching.consider_vehicle_location"):
        context.stage("matching.tours.filtered")
    context.stage("matching.selected", alias="groups")
    context.stage("filtering.carpoolable")


def execute(context: synpp.ExecuteContext):
    """Execute."""
    stats = dict()
    driver_carpoolable, passenger_carpoolable = context.stage(
        "filtering.carpoolable"
    )
    trips = context.stage("synthesis.metadata")
    stats["passenger_carpoolable_count"] = driver_carpoolable.sum()
    stats["driver_carpoolable_count"] = passenger_carpoolable.sum()
    stats["passenger_carpoolable_vmt"] = trips.loc[
        passenger_carpoolable, "routed_distance"
    ].sum()
    stats["driver_carpoolable_vmt"] = trips.loc[
        driver_carpoolable, "routed_distance"
    ].sum()
    graph = context.stage("graph")
    if context.config("matching.consider_vehicle_location"):
        tours = context.stage("matching.tours.filtered")
    groups = context.stage("groups")

    stats["trip_count"] = trips.shape[0]

    car_trips = trips.loc[trips["mode"].eq("car")]
    stats["car_trips_count"] = car_trips.shape[0]
    stats["total_car_vmt"] = car_trips["routed_distance"].sum()

    carpoolable = driver_carpoolable | passenger_carpoolable
    stats["carpoolable_vmt"] = trips.loc[carpoolable, "routed_distance"].sum()

    stats["total_carpoolable_count"] = carpoolable.sum()

    stats["routes_count"] = (
        trips["routed_distance"].notna() & carpoolable
    ).sum()
    stats["routes_length"] = trips["routed_distance"].sum()

    shareable_trips_idx = get_shareable_trips_idx(graph)

    stats["shareable_vmt"] = trips.loc[
        list(shareable_trips_idx), "routed_distance"
    ].sum()
    stats["shareable_trips_count"] = len(shareable_trips_idx)

    passenger_trips = pd.Index(
        itertools.chain(*[group[1:] for group in groups])
    )
    if context.config("matching.consider_vehicle_location"):
        stats["tours_count"] = len(tours)
        trips_in_tour = [item for sublist in tours for item in sublist]
        stats["tours_vmt"] = trips.loc[trips_in_tour, "routed_distance"].sum()

        set_passenger_trips = set(passenger_trips)
        passengeable_tours = [
            tour for tour in tours if set(tour).issubset(set_passenger_trips)
        ]
        stats["passengeable_tours_count"] = len(passengeable_tours)
        trips_in_passengeable_tour = [
            item for sublist in passengeable_tours for item in sublist
        ]
        stats["passenger_tours_vmt"] = trips.loc[
            trips_in_passengeable_tour, "routed_distance"
        ].sum()

    driver_trips = pd.Index([group[0] for group in groups])
    stats["driver_vmt"] = trips.loc[driver_trips, "routed_distance"].sum()
    stats["dtrips_count"] = len(driver_trips)

    stats["passenger_vmt"] = trips.loc[
        passenger_trips, "routed_distance"
    ].sum()
    stats["ptrips_count"] = len(passenger_trips)

    matched_trips = passenger_trips.union(driver_trips)
    stats["matched_vmt"] = trips.loc[matched_trips, "routed_distance"].sum()
    stats["matched_trips_count"] = len(matched_trips)

    return stats
