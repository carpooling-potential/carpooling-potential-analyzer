# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
# 
# aina.rasoldier@inria.fr
# 
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Batch execution of matching stats."""
import os
import math

from matplotlib import pyplot as plt

import synpp


def get_config(seed, rate, maximal):
    return {
        "subsampling": {"seed": seed, "rate": rate},
        "shareability_preparation": {
            "identical": {"activate": True},
            "inclusive": {"activate": False},
        },
        "matching": {
            "passenger_capacity": 1,
            "maximal_matching": maximal,
            "variable_load": False,
            "consider_vehicle_location": False,
        },
    }


def configure(context: synpp.ConfigurationContext):
    """Configure."""
    context.config("paths.output")
    context.config("synthesis.sampling_rate")
    context.config("batch.sampling_rates")
    context.config("batch.seed_count")
    for maximal in (True, False):
        for rate in context.config("batch.sampling_rates"):
            for seed in range(context.config("batch.seed_count")):
                context.stage(
                    "matching.selected",
                    alias=(seed, rate, maximal),
                    config=get_config(seed, rate, maximal),
                )


def execute(context: synpp.ExecuteContext):
    """Execute."""
    rates = context.config("batch.sampling_rates")
    stats = {
        True: {r: dict() for r in rates},
        False: {r: dict() for r in rates},
    }
    for maximal in (True, False):
        for rate in rates:
            for seed in range(context.config("batch.seed_count")):
                stats[maximal][rate][seed] = context.get_info(
                    (seed, rate, maximal),
                    "avoided_vmt"
                )

    means = []
    stds = []
    rates = context.config("batch.sampling_rates")
    seed_count = context.config("batch.seed_count")
    for r in rates:
        sum_ratio = 0
        for s in range(seed_count):
            ratio = stats[False][r][s] / stats[True][r][s]
            sum_ratio += ratio
        mean_ratio = sum_ratio / seed_count
        means.append(mean_ratio)

        sum_variance = 0
        for s in range(seed_count):
            sum_variance += (ratio - means[-1]) ** 2
        std = math.sqrt(sum_variance / seed_count)
        stds.append(std)

    # plt.plot(rates, means)
    plt.errorbar(
        [(r * context.config("synthesis.sampling_rate") * 100) for r in rates],
        means,
        stds,
    )
    plt.title("Quality of the solution of the matching algorithm")
    plt.ylabel("Algorithm solution/optimal solution")
    plt.xlabel("Sampling rate (%)")
    plt.savefig(
        os.path.join(context.config("paths.output"), "matching_quality.svg")
    )
