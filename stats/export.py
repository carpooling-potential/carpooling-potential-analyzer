# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
# 
# aina.rasoldier@inria.fr
# 
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Output carpooling trips to geopackage files."""

import os

import geopandas as gpd

from commons import catchtime, get_run_output_path


def configure(context):
    """Configure."""
    context.config("paths.output")
    context.stage("synthesis.routed")
    context.stage("matching.selected", alias="groups")
    context.stage("filtering.carpoolable")
    context.stage("filtering.subsample")
    context.stage("matching.tours.filtered")


def execute(context):
    """Execute."""
    groups = context.stage("groups")
    synthesis = context.stage("synthesis.routed")
    passenger_carpoolable, driver_carpoolable = context.stage(
        "filtering.carpoolable"
    )
    subsample = context.stage("filtering.subsample")

    synthesis["group_id"] = None
    synthesis["driver_carpoolable"] = driver_carpoolable
    synthesis["passenger_carpoolable"] = passenger_carpoolable
    synthesis["in_subsample"] = subsample

    synthesis["status"] = None
    synthesis["identical"] = False
    synthesis["inclusive"] = False

    all_passengers = []
    for group_id, group in context.progress(
        enumerate(groups),
        label="Distributing groups to trips",
        total=len(groups),
    ):
        driver = group[0]
        passengers = group[1:]
        all_passengers.extend(passengers)

        synthesis.loc[group, "group_id"] = group_id
        synthesis.loc[driver, "status"] = "driver"
        synthesis.loc[passengers, "status"] = "passenger"

    all_passengers = set(all_passengers)

    # Tours
    tours = context.stage("matching.tours.filtered")
    for tour_idx, tour in context.progress(
        reversed(list(enumerate(tours))),
        label="Tours matching order",
        total=len(tours),
    ):
        if set(tour).issubset(all_passengers):
            synthesis.loc[tour, "matching_order"] = tour_idx

    with catchtime("Creating GeoDataFrame"):
        gdf_trips = gpd.GeoDataFrame(
            synthesis[
                [
                    "route",
                    "mode",
                    "preceding_purpose",
                    "following_purpose",
                    "departure_time",
                    "arrival_time",
                    "driver_carpoolable",
                    "passenger_carpoolable",
                    "departure_constrained",
                    "group_id",
                    "status",
                    "identical",
                    "inclusive",
                    "matching_order",
                    "in_subsample",
                ]
            ],
            geometry="route",
        )

    output_path = get_run_output_path(context)
    with catchtime("Export carpooling trips to GPKG file..."):
        gdf_trips.to_file(
            os.path.join(output_path, "trips.gpkg"),
            driver="GPKG",
        )
