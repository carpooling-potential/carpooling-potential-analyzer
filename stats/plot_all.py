# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
#
# aina.rasoldier@inria.fr
#
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
"""Batch execution of matching stats."""
import os
import math
import itertools
import numpy as np

from matplotlib import pyplot as plt
from matplotlib import ticker as mtick

import synpp

from commons import get_run_output_path

import synthesis.geofiltered
import filtering.carpoolable

COMMUTE_TRIPS = {
    "carpoolable": {
        "driver": [
            ["home", "work"],
            ["work", "home"],
            ["home", "education"],
            ["education", "home"],
        ],
        "passenger": [
            ["home", "work"],
            ["home", "education"],
            ["work", "home"],
            ["education", "home"],
        ],
    }
}
ALL_TRIPS = {
    "carpoolable": {
        "driver": "all",
        "passenger": "all",
    }
}

PARAMETER_SELECTION = {
    "stats.batch.rate": {
        "name": "taux d'adoption",
        "xlabel": "$s$",
        "formatter": "percent"
    },
    "stats.batch.capacity": {
        "name": "capacité",
        "xlabel": "$c$",
        "formatter": None
    },
    "stats.batch.time_constraint": {
        "name": "flexibilité temporelle",
        "xlabel": "$\\delta^{t}$ (s)",
        "formatter": None
    },
    "stats.batch.space_constraint": {
        "name": "flexibilité spatiale",
        "xlabel": "$\\delta^{\\mathrm{dist}}$ (m)",
        "formatter": None
    },
}

SCENARIOS = {
    "all_d2d": {
        "meta": {
            "label": "Tous les trajets, covoiturage à l'identique",
            "linestyle": "dashed",
            "marker": ".",
            "color": "C1",
        },
        "shareability_preparation": {"inclusive": {"activate": False}},
        "matching": {
            "consider_vehicle_location": True,
        },
        **ALL_TRIPS,
    },
    "all_d2dpudo": {
        "meta": {
            "label": "Tous les trajets, covoiturage à l'identique et partiel",
            "linestyle": "solid",
            "marker": ".",
            "color": "C1",
        },
        "shareability_preparation": {"inclusive": {"activate": True}},
        "matching": {
            "consider_vehicle_location": True,
        },
        **ALL_TRIPS,
    },
    "all_d2dpudo_notour": {
        "meta": {
            "label": "Tous les trajets, covoiturage à l'identique et partiel (circuits ignorés)",
            "linestyle": "dashdot",
            "marker": "+",
            "color": "C1",
        },
        "shareability_preparation": {"inclusive": {"activate": True}},
        "matching": {
            "consider_vehicle_location": False,
        },
        **ALL_TRIPS,
    },
    "commute_d2d": {
        "meta": {
            "label": "Trajets pendulaires, covoiturage à l'identique",
            "linestyle": "dashed",
            "marker": ".",
            "color": "C2",
        },
        "shareability_preparation": {"inclusive": {"activate": False}},
        "matching": {
            "consider_vehicle_location": True,
        },
        **COMMUTE_TRIPS,
    },
    "commute_d2dpudo": {
        "meta": {
            "label": "Trajets pendulaires, covoiturage à l'identique et partiel",
            "linestyle": "solid",
            "marker": ".",
            "color": "C2",
        },
        "shareability_preparation": {"inclusive": {"activate": True}},
        "matching": {
            "consider_vehicle_location": True,
        },
        **COMMUTE_TRIPS,
    },
    "commute_d2dpudo_notour": {
        "meta": {
            "label": "Trajets pendulaires, covoiturage à l'identique et partiel (circuits ignorés)",
            "linestyle": "dashdot",
            "marker": "+",
            "color": "C2",
        },
        "shareability_preparation": {"inclusive": {"activate": True}},
        "matching": {
            "consider_vehicle_location": False,
        },
        **COMMUTE_TRIPS,
    },
}


def configure(context: synpp.ConfigurationContext):
    """Configure."""
    for param in PARAMETER_SELECTION:
        context.stage(param)

    context.config("paths.output")
    context.stage(synthesis.geofiltered)
    context.stage(
        filtering.carpoolable, COMMUTE_TRIPS, alias="commute_carpoolable"
    )
    context.stage(filtering.carpoolable, ALL_TRIPS, alias="all_carpoolable")
    context.stage("matching.selected")


def plot_scenarios(ax: plt.Axes, scenarios, data):
    avoided = {}
    for v in data.values():
        for s_name in scenarios:
            if s_name not in avoided:
                avoided[s_name] = []
            avoided[s_name].append(float(v[s_name]) / 1000)
    param_values = list(map(float, data.keys()))

    for scenario_id in avoided:
        ax.plot(
            param_values,
            avoided[scenario_id],
            label=scenarios[scenario_id]["meta"]["label"],
            marker=scenarios[scenario_id]["meta"]["marker"],
            color=scenarios[scenario_id]["meta"]["color"],
            linestyle=scenarios[scenario_id]["meta"]["linestyle"],
        )
    ax.set_xticks(param_values)


def format_axes(ax_abs, carpoolable_vmt):
    _ymin, ymax = ax_abs.get_ylim()
    ax_rel = ax_abs.twinx()
    ax_rel.set_ylim(top=ymax / (carpoolable_vmt / 1000))
    ax_rel.set_zorder(0)
    ax_rel.yaxis.set_major_formatter(mtick.PercentFormatter(xmax=1))
    ax_rel.set_ylim(bottom=0)
    ax_rel.set_ylabel("km évités en part des km voiture")
    ax_rel.yaxis.tick_left()
    ax_rel.yaxis.set_label_position("left")
    ax_abs.yaxis.tick_right()
    ax_abs.yaxis.set_label_position("right")
    ax_abs.grid()
    ax_abs.set_xlim(left=0)
    ax_abs.set_ylim(bottom=0)
    ax_abs.set_zorder(3)
    ax_abs.set_ylabel("km évités")
    ax_abs.patch.set_facecolor(None)


def plot_theo_max_capacity(ax: plt.Axes, shareable_vmt):
    x = np.array([0, 1, 2, 3, 4])
    y = (x / (x + 1)) * shareable_vmt / 1000
    (theoretical,) = ax.plot(x, y, linestyle=":", marker=".")
    ax.legend([theoretical], ["Max. théo. de km évités"])


def execute(context: synpp.ExecuteContext):
    # """Execute."""
    output_path = get_run_output_path(context)
    carpoolable_vmt = context.get_info("all_carpoolable", "carpoolable_vmt")
    shareable_vmt = context.get_info("matching.selected", "shareable_vmt")

    plt.rc(
        "pgf",
        rcfonts=False,
        preamble=r"""
        \usepackage[fontsize=10pt]{scrextend}
        \usepackage{unicode-math}
    """,
    )  # https://github.com/matplotlib/matplotlib/issues/26892

    fig, axes = plt.subplots(
        math.ceil(len(PARAMETER_SELECTION) / 2), 2, figsize=(8, 6)
    )
    fig.subplots_adjust(
        left=0, bottom=0, right=1, top=1, wspace=0.6, hspace=0.3
    )
    for ax, (param_id, param_meta) in zip(
        itertools.chain.from_iterable(axes), PARAMETER_SELECTION.items()
    ):
        data = context.stage(param_id)
        plot_scenarios(ax, SCENARIOS, data)
        ax.set_xlabel(param_meta["xlabel"])
        ax.set_title(param_meta["name"])
        if param_meta["formatter"] == "percent" :
            ax.xaxis.set_major_formatter(mtick.PercentFormatter(xmax=1))

        if param_id == "stats.batch.capacity":
            _xmin, xmax = ax.get_xlim()
            ax.set_xticks(range(int(xmax) + 1))
            plot_theo_max_capacity(ax, shareable_vmt)
        format_axes(ax, carpoolable_vmt)

    fig.legend(
        *ax.get_legend_handles_labels(),
        loc="upper center",
        bbox_to_anchor=(0.5, -0.1),
        ncols=2,
    )
    for format in ("pgf", "pdf"):
        fig.savefig(
            os.path.join(output_path, f"figure.{format}"),
            bbox_inches="tight",
            transparent=True,
        )
