# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
#
# aina.rasoldier@inria.fr
#
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Stage to print statistics about carpooling potential."""

import json

import numpy as np

from commons import EscapeSequences


class NumpyEncoder(json.JSONEncoder):
    """Custom encoder for numpy data types."""

    def default(self, obj):
        """Encode."""
        if isinstance(
            obj,
            (
                np.int_,
                np.intc,
                np.intp,
                np.int8,
                np.int16,
                np.int32,
                np.int64,
                np.uint8,
                np.uint16,
                np.uint32,
                np.uint64,
            ),
        ):
            return int(obj)

        elif isinstance(obj, (np.float_, np.float16, np.float32, np.float64)):
            return float(obj)

        elif isinstance(obj, (np.complex_, np.complex64, np.complex128)):
            return {"real": obj.real, "imag": obj.imag}

        elif isinstance(obj, (np.ndarray,)):
            return obj.tolist()

        elif isinstance(obj, (np.bool_)):
            return bool(obj)

        elif isinstance(obj, (np.void)):
            return None

        return json.JSONEncoder.default(self, obj)


def pprint_val(
    name: str,
    count: int,
    length: float = None,
    total_length: float = None,
    total_count: int = None,
) -> None:
    """Just a function to pretty print stats."""
    infos = [f"{count:,}"]
    if total_count is not None:
        infos.append(
            EscapeSequences.LIGHTBLUE
            + f"{count / total_count * 100:,.2f}%"
            + EscapeSequences.ENDC
        )

    if length is not None:
        infos.append(
            EscapeSequences.BOLD
            + f"Average length: {length/1000/count:,.2f}km"
            + EscapeSequences.ENDC
        )
        infos.append(
            EscapeSequences.LIGHTGREEN
            + f"{length/1000:,.2f}km"
            + EscapeSequences.ENDC
        )

        if total_length is not None:
            infos.append(
                EscapeSequences.LIGHTYELLOW
                + f"{length / total_length * 100:,.2f}%"
                + EscapeSequences.ENDC
            )

    print(
        ">>>> "
        + EscapeSequences.LIGHTCYAN
        + name
        + EscapeSequences.ENDC
        + ": "
        + " / ".join(infos)
    )


def configure(context):
    """Configure."""
    context.stage("stats.metrics")


def execute(context):
    """Execute."""
    stats = context.stage("stats.metrics")
    print(
        EscapeSequences.LIGHTMAGENTA
        + "CARPOOLING POTENTIAL STATS"
        + EscapeSequences.ENDC
    )

    if context.config("matching.consider_vehicle_location"):
        print(
            EscapeSequences.LIGHTBLUE
            + "Considering vehicle location"
            + EscapeSequences.ENDC
        )
    else:
        print(
            EscapeSequences.LIGHTYELLOW
            + "NOT considering vehicle location"
            + EscapeSequences.ENDC
        )
    print("Passenger capacity:", context.config("matching.passenger_capacity"))
    print("Sampling:", context.config("subsampling.rate"))
    pprint_val("Total trips from synthetic population", stats["trip_count"])
    pprint_val(
        "Total car trips in synthetic population",
        stats["car_trips_count"],
        total_count=stats["trip_count"],
        length=stats["total_car_vmt"],
    )
    pprint_val(
        "Passenger carpoolable trips",
        stats["passenger_carpoolable_count"],
        length=stats["passenger_carpoolable_vmt"],
        total_count=stats["trip_count"],
    )
    pprint_val(
        "Driver carpoolable trips",
        stats["driver_carpoolable_count"],
        length=stats["driver_carpoolable_vmt"],
        total_count=stats["trip_count"],
    )
    pprint_val(
        "Total carpoolable trips",
        stats["total_carpoolable_count"],
        length=stats["carpoolable_vmt"],
        total_count=stats["trip_count"],
    )
    pprint_val(
        "Trips with route (wrt unary filtered trips)",
        stats["routes_count"],
        length=stats["routes_length"],
        total_count=stats["total_carpoolable_count"],
    )
    pprint_val(
        "Shareable trips after binary filtering",
        stats["shareable_trips_count"],
        length=stats["shareable_vmt"],
        total_count=stats["total_carpoolable_count"],
    )
    if context.config("matching.consider_vehicle_location"):
        pprint_val(
            "Tours",
            stats["tours_count"],
            length=stats["tours_vmt"],
        )
        pprint_val(
            "Passengeable tours",
            count=stats["passengeable_tours_count"],
            length=stats["passenger_tours_vmt"],
        )
    pprint_val(
        "Driver trips", stats["dtrips_count"], length=stats["driver_vmt"]
    )
    pprint_val(
        "Passenger trips",
        stats["ptrips_count"],
        length=stats["passenger_vmt"],
    )
    pprint_val(
        "Matched trips",
        stats["matched_trips_count"],
        length=stats["matched_vmt"],
    )

    print(
        ">>>> Avg traveler / carpooled vkt:",
        round(stats["matched_vmt"] / stats["driver_vmt"], 3),
    )

    print(
        ">>>> Avg traveler / total vkt:",
        round(
            stats["total_car_vmt"]
            / (stats["total_car_vmt"] - stats["passenger_vmt"]),
            3,
        ),
    )
