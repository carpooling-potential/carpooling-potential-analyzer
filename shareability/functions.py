# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
# 
# aina.rasoldier@inria.fr
# 
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Functions that are common to any carpooling form."""
import pandas as pd


def get_relative_filtered_pairs_mask(
    pairs: pd.DataFrame, trips: pd.DataFrame, relative_constraints: dict
):
    # Join on source node, i.e., trip as potential passenger.
    pairs = pairs.join(
        trips[["routed_distance", "duration_computed"]], on="source"
    )

    # Relative distances between origins
    relative_distance_origin = (
        pairs["origin_distance"] / pairs["routed_distance"]
    )

    # Relative distances between destinations
    relative_distance_destination = (
        pairs["destination_distance"] / pairs["routed_distance"]
    )

    space_mask = relative_distance_origin.le(
        relative_constraints["space_constraint"]["origin"]
    ) & relative_distance_destination.le(
        relative_constraints["space_constraint"]["destination"]
    )

    relative_departure_diff = (
        pairs["departure_time_difference"] / pairs["duration_computed"]
    )
    departure_mask = pairs[
        "departure_constrained"
    ] & relative_departure_diff.between(
        relative_constraints["time_constraint"]["origin"][0],
        relative_constraints["time_constraint"]["origin"][1],
    )

    relative_arrival_diff = (
        pairs["arrival_time_difference"] / pairs["duration_computed"]
    )
    arrival_mask = ~pairs[
        "departure_constrained"
    ] & relative_arrival_diff.between(
        relative_constraints["time_constraint"]["destination"][0],
        relative_constraints["time_constraint"]["destination"][1],
    )

    return space_mask & (departure_mask | arrival_mask)


def get_space_mask(pairs: pd.DataFrame, space_constraint: dict):
    """
    Create a mask keeping only the pairs respecting the constraints.

    Only checks if origin/destination distances and time differences are under
    a given value.
    Potential TODO: The logic could be different given the form.
    """
    return (
        pairs["origin_distance"].le(space_constraint["origin"])
        & pairs["destination_distance"].le(space_constraint["destination"])
    )


def get_time_mask(pairs: pd.DataFrame, time_constraint: dict):
    """
    Create a mask keeping only the pairs respecting the constraints.

    Only checks if origin/destination distances and time differences are under
    a given value.
    Potential TODO: The logic could be different given the form.
    """
    departure_match = (
        pairs["departure_time_difference"].between(
            time_constraint["origin"][0],
            time_constraint["origin"][1],
        )
        & pairs["departure_constrained"]
    )

    arrival_match = (
        pairs["arrival_time_difference"].between(
            time_constraint["destination"][0],
            time_constraint["destination"][1],
        )
        & ~pairs["departure_constrained"]
    )
    # If any of the two is matching: match! No priority on driver or passenger.
    return departure_match | arrival_match
