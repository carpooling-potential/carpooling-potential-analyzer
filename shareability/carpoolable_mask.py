# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
# 
# aina.rasoldier@inria.fr
# 
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Get binary mask (filter) for carpoolable trips."""
import pandas as pd

def configure(context):
    context.stage("shareability.merged")
    context.stage("filtering.carpoolable")


def get_carpoolable_pairs_mask(
    pairs: pd.DataFrame,
    driver_carpoolable: pd.Series,
    passenger_carpoolable: pd.Series,
):
    print("Number of edges:", pairs.shape[0])

    driver_carpoolable_idx = driver_carpoolable.loc[driver_carpoolable].index
    mask = pairs["target"].isin(driver_carpoolable_idx)
    print(mask.sum(), "edges are driver carpoolable")

    passenger_carpoolable_idx = passenger_carpoolable.loc[
        passenger_carpoolable
    ].index
    mask &= pairs["source"].isin(passenger_carpoolable_idx)
    print(len(mask), "edges are passenger carpoolable")

    print("Carpoolable edges", mask.sum())
    return mask


def get_subsampled_pairs_mask(
    pairs: pd.DataFrame, subsample_mask: "pd.Series[bool]"
):
    print("Number of edges:", pairs.shape[0])

    subsampled_trips_idx = subsample_mask.loc[subsample_mask].index
    mask = pairs["target"].isin(subsampled_trips_idx) & pairs["source"].isin(
        subsampled_trips_idx
    )
    print(mask.sum(), "edges are in the subsample")

    return mask


def execute(context):
    driver_carpoolable, passenger_carpoolable = context.stage(
        "filtering.carpoolable"
    )

    pairs = context.stage("shareability.merged")

    return get_carpoolable_pairs_mask(
        pairs, driver_carpoolable, passenger_carpoolable
    )
