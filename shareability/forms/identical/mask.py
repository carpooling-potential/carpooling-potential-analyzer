# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
# 
# aina.rasoldier@inria.fr
# 
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""
Get binary mask (filter) for pairs matching the constraints.

TODO: relative filter
"""
import pandas as pd

from shareability.functions import get_space_mask, get_time_mask
from commons import catchtime

FORM = "identical"


def configure(context):
    context.stage(f"shareability.forms.{FORM}.raw_pairs", alias="pairs")
    context.config(f"shareability.{FORM}")


def execute(context):
    pairs = context.stage("pairs")
    config = context.config(f"shareability.{FORM}")

    print("Initial number of pairs:", pairs.shape[0])

    mask = pd.Series(False, index=pairs.index)
    print("Mask size:", mask.shape[0])
    with catchtime(f'Filtering "{FORM}" pairs on space and time constraints'):
        mask |= get_space_mask(
            pairs, config["filter"]["space_constraint"]
        ) & get_time_mask(pairs, config["filter"]["time_constraint"])

    print("Count of kept pairs:", mask.sum())
    return mask
