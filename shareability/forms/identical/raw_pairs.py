# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
# 
# aina.rasoldier@inria.fr
# 
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""
Binary filtering is the process where trips are compared pairwise.

The current only binary filtering is on difference between time of departure
and distance between origin and destination points of trips.
The output of this step is a shareability graph, i.e. a graph where trips are
node and edges link nodes which are shareable in time and space.
"""

import numpy as np
import pandas as pd
import geopandas as gpd
from scipy.spatial import cKDTree
from typing import Callable

import synpp

from commons import catchtime, time_difference


COLS = [
    "source",
    "target",
    "origin_distance",
    "destination_distance",
    "arrival_time_difference",
    "departure_time_difference",
    "departure_constrained",
    "departure_constrained_target",
]


def configure(context: synpp.ConfigurationContext):
    """Configure."""
    context.config("shareability_preparation.identical.max")
    context.config("shareability_preparation.separate_departure_constrained")
    context.stage("synthesis.metadata")
    context.config("carpoolable.modes")


def extract_points(trips, col):
    """
    Extract origin points, destination points and departure time.

    Extraction is done in numpy arrays.
    Departure times are extracted in a (n,1) dimension matrix for convenient
    pairwise distance calculation.
    """
    return np.stack(trips[col].apply(lambda g: g.coords[0]))


def query_pairs(points, constraint):
    """Create match matrix with cKDTree."""
    print("Creating the kd-tree...")
    tree = cKDTree(points)
    print("Querying pairs...")
    pairs = tree.query_pairs(constraint, output_type="ndarray")
    return pairs


def process_pairs_chunk(pairs, trips, constraints):
    pairs = gpd.GeoDataFrame(
        pairs.join(trips, on="source").join(
            trips, on="target", rsuffix="_target"
        )
    )
    pairs = pairs.loc[pairs["person_id"].ne(pairs["person_id_target"])]

    pairs["departure_time_difference"] = time_difference(
        pairs["departure_time_target"], pairs["departure_time"]
    )

    pairs = pairs.loc[
        pairs["departure_time_difference"]
        .abs()
        .le(constraints["time_constraint"])
    ]

    pairs["arrival_time_difference"] = time_difference(
        pairs["arrival_time_target"], pairs["arrival_time"]
    )

    pairs = pairs.loc[
        pairs["arrival_time_difference"]
        .abs()
        .le(constraints["time_constraint"])
    ]

    pairs["destination_distance"] = pairs["proj_following_geometry"].distance(
        pairs["proj_following_geometry_target"]
    )

    pairs = pairs.loc[
        pairs["destination_distance"].le(constraints["space_constraint"])
    ]

    pairs["origin_distance"] = pairs["proj_preceding_geometry"].distance(
        pairs["proj_preceding_geometry_target"]
    )

    return pairs.astype(
        {
            "departure_constrained": bool,
            "departure_constrained_target": bool,
        }
    )


def get_pairs(
    progress: Callable[..., synpp.ProgressContext],
    trips: pd.DataFrame,
    constraints: dict,
):
    """
    Make a shareability graph thanks to kdtree (v3).

    Instead of creating a shareability matrix, work on pairs after querying
    the kdtree.
    """
    origins = extract_points(trips, "proj_preceding_geometry")
    with catchtime("Querying pairs on KDTree for origin space constraint"):
        init_pairs = query_pairs(origins, constraints["space_constraint"])

    chunksize = int(1e6)
    pairs_list = []
    with progress(
        total=init_pairs.shape[0], label="Potential identical carpooling pairs"
    ) as p:
        for i in range(0, init_pairs.shape[0], chunksize):
            source = trips.index.values[init_pairs[i : i + chunksize, 0]]
            target = trips.index.values[init_pairs[i : i + chunksize, 1]]
            init_pairs_chunk = pd.DataFrame(
                np.column_stack((source, target)), columns=["source", "target"]
            )
            pairs = process_pairs_chunk(init_pairs_chunk, trips, constraints)
            if pairs.shape[0] > 0:
                pairs_list.append(pairs[COLS])
            p.update(chunksize)

    return pd.concat(pairs_list, ignore_index=True)


def execute(context: synpp.ExecuteContext):
    """Execute."""
    trips: pd.DataFrame = context.stage("synthesis.metadata")
    # Filter here because of memory allocation problem.
    print("Filter on mode. Initial number of trips:", trips.shape[0])
    trips = trips.loc[trips["mode"].isin(context.config("carpoolable.modes"))]
    print("Making a KD-Tree on", trips.shape[0], "trips")

    constraints: dict = context.config(
        "shareability_preparation.identical.max"
    )

    trips = trips[
        [
            "person_id",
            "proj_preceding_geometry",
            "proj_following_geometry",
            "departure_time",
            "arrival_time",
            "departure_constrained",
        ]
    ]

    if context.config(
        "shareability_preparation.separate_departure_constrained"
    ):
        with catchtime("Computing shareability graph (departure)"):
            pairs_departure_constrained = get_pairs(
                context.progress,
                trips.loc[trips["departure_constrained"]],
                constraints,
            )

        with catchtime("Computing shareability graph (arrival)"):
            pairs_constraint_on_arrival = get_pairs(
                context.progress,
                trips.loc[~trips["departure_constrained"]],
                constraints,
            )

        pairs = pd.concat(
            [pairs_departure_constrained, pairs_constraint_on_arrival]
        )
    else:
        pairs = get_pairs(context.progress, trips, constraints)

    pairs["form"] = "identical"

    return pairs
