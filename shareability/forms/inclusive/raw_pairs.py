# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
#
# aina.rasoldier@inria.fr
#
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""
Construct a pairwise shareability graph of trips for inclusive carpooling.

There, each edge is a match possibility in the inclusive carpooling form.
"""

from commons import catchtime
import os

import pandas as pd
import geopandas as gpd
import numpy as np

import synpp

from commons import print_colored, EscapeSequences
from shareability.forms.inclusive.worker import worker_wrapper


def configure(context: synpp.ConfigurationContext):
    context.config("shareability_preparation.inclusive.max")
    context.config("shareability_preparation.separate_departure_constrained")
    context.config("shareability_preparation.inclusive.trips_per_job")
    context.stage("synthesis.routed")


def find_pairs_within_distance_mp(context: synpp.ExecuteContext, trips):
    constraints = context.config("shareability_preparation.inclusive.max")

    # Create square bounds around origin and destination points.
    bounds_preceding = trips["proj_preceding_geometry"].buffer(
        constraints["space_constraint"], cap_style=3
    )
    bounds_following = trips["proj_following_geometry"].buffer(
        constraints["space_constraint"], cap_style=3
    )

    # Create R-Tree indexes for origin and destination.
    rtree_preceding = bounds_preceding.sindex
    rtree_following = bounds_following.sindex

    # Group batches for multiproccessing jobs.
    n_trips_per_job = context.config(
        "shareability_preparation.inclusive.trips_per_job"
    )
    njobs = trips.shape[0] / n_trips_per_job
    groups = np.array_split(trips.index, njobs)
    try:
        with context.progress(
            label="computing pairs", total=trips.shape[0]
        ), context.parallel(
            data={
                "trips": trips,
                "cache_path": context.cache_path,
                "constraints": constraints,
                "rtree_preceding": rtree_preceding,
                "rtree_following": rtree_following,
            },
            maxtasksperchild=1,
        ) as pool:
            list(pool.imap_unordered(worker_wrapper, groups))
    except Exception as e:
        raise e

    cache_files = os.listdir(context.cache_path)
    pairs = [
        pd.read_pickle(os.path.join(context.cache_path, f))
        for f in context.progress(
            cache_files, total=len(cache_files), label="Reading cache files"
        )
    ]
    return pd.concat(pairs)


def execute(context: synpp.ExecuteContext):
    trips: gpd.GeoDataFrame = context.stage("synthesis.routed")
    print_colored(EscapeSequences.LIGHTBLUE, f"{trips.shape[0]} trips")

    with catchtime("Deleting trips with no route"):
        trips = trips.loc[trips["route"].notna()]
        assert not trips.empty
    print_colored(EscapeSequences.LIGHTBLUE, f"{trips.shape[0]} trips")

    with catchtime("Deleting trips with distance 0 route"):
        trips = trips.loc[trips["routed_distance"].gt(0)].copy()
    print_colored(EscapeSequences.LIGHTBLUE, f"{trips.shape[0]} trips")

    trips["route"] = trips["route"].to_crs(epsg=2154)

    pairs = find_pairs_within_distance_mp(
        context,
        trips.loc[
            trips["mode"].eq("car"),
            [
                "person_id",
                "proj_preceding_geometry",
                "proj_following_geometry",
                "departure_time",
                "arrival_time",
                "timing",
                "route",
                "departure_constrained",
            ],
        ],
    )

    pairs["form"] = "inclusive"

    print("Pair number", pairs.shape[0])

    # Shared DataFrame messes the types. Need to force the bool.
    return pairs.reset_index(drop=True).sort_values(["source", "target"])
