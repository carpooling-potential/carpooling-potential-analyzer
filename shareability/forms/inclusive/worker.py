# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
# 
# aina.rasoldier@inria.fr
# 
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Worker function for pairs computation."""
from geopandas import GeoSeries, GeoDataFrame
from numpy import interp, concatenate
from pandas import concat, Index, merge

import os
import warnings

from commons import time_difference


def get_passage(trips: GeoDataFrame, driver_route, timing):
    """
    Get departure and arrival passages of trips on route_d.

    Also remove trips that do not got on the same direction.
    """
    trips = trips.copy()
    # Projecting potential passenger's origins and destinations on potential
    # driver's route.
    geoms = concat(
        [trips["proj_preceding_geometry"], trips["proj_following_geometry"]]
    )
    geoms = GeoSeries(geoms).values
    with warnings.catch_warnings():
        warnings.filterwarnings(
            "ignore", "invalid value encountered in line_locate_point"
        )
        projs = driver_route.project(geoms)

    # Filtering pairs, keeping only those that go to the same direction.
    orig_projs = projs[: trips.shape[0]]
    dest_projs = projs[trips.shape[0] :]
    mask = orig_projs < dest_projs
    trips = trips.loc[mask]

    passages = interp(
        concatenate((orig_projs[mask], dest_projs[mask])),
        timing[:, 0],
        timing[:, 1],
    )
    trips["departure_passage"] = passages[: trips.shape[0]]
    trips["arrival_passage"] = passages[trips.shape[0] :]
    return trips[["departure_passage", "arrival_passage"]]


def worker_wrapper(context, group_indices):
    find_pairs_in_group(
        context.data("trips"),
        context.data("constraints"),
        context.progress,
        context.data("rtree_preceding"),
        context.data("rtree_following"),
        group_indices,
    ).to_pickle(
        os.path.join(context.data("cache_path"), str(group_indices[0]) + ".p")
    )


def find_pairs_in_group(
    trips,
    constraints,
    progress,
    rtree_preceding,
    rtree_following,
    group_indices,
):
    # Iterating over the potential drivers
    # For each potential driver, find their potential passengers
    pairs = []
    for driver_idx, route, timing, person_id in trips.loc[
        group_indices, ["route", "timing", "person_id"]
    ].itertuples(name=None):
        # Getting the index of passenger trips with close orig and dest
        # Candidates passengers first found by r-tree
        pairs_part = trips.iloc[
            Index(rtree_preceding.intersection(route.bounds)).intersection(
                Index(rtree_following.intersection(route.bounds))
            )
        ]

        # Removing same person trips
        pairs_part = pairs_part.loc[pairs_part["person_id"].ne(person_id)]

        pairs_part = get_passage(pairs_part, route, timing)
        pairs_part["target"] = driver_idx
        pairs.append(
            pairs_part[["target", "departure_passage", "arrival_passage"]]
        )
        progress.update()

    pairs = concat(pairs).reset_index(names="source")
    pairs = merge_pairs(trips, pairs)
    pairs = assign_distances(trips, pairs)
    pairs = filter_constraints(constraints, pairs)

    return pairs[
        [
            "source",
            "target",
            "origin_distance",
            "destination_distance",
            "departure_time_difference",
            "arrival_time_difference",
            "departure_constrained",
            "departure_constrained_target",
        ]
    ]


def merge_pairs(trips, pairs):
    pairs = merge(
        pairs,
        trips[
            [
                "proj_preceding_geometry",
                "proj_following_geometry",
                "departure_time",
                "arrival_time",
                "departure_constrained",
            ]
        ],
        how="left",
        left_on="source",
        right_index=True,
    )
    pairs = merge(
        pairs,
        trips[["route", "departure_time", "departure_constrained"]],
        how="left",
        left_on="target",
        right_index=True,
        suffixes=(None, "_target"),
    )
    return pairs


def assign_distances(trips, pairs):
    pairs["origin_distance"] = GeoSeries(
        pairs["proj_preceding_geometry"]
    ).distance(GeoSeries(pairs["route"]))
    pairs["destination_distance"] = GeoSeries(
        pairs["proj_following_geometry"]
    ).distance(GeoSeries(pairs["route"]))

    pairs["departure_time_difference"] = time_difference(
        trips.loc[pairs["target"], "departure_time"].values
        + pairs["departure_passage"].values,
        trips.loc[pairs["source"], "departure_time"].values,
    )
    pairs["arrival_time_difference"] = time_difference(
        trips.loc[pairs["target"], "departure_time"].values
        + pairs["arrival_passage"].values,
        trips.loc[pairs["source"], "arrival_time"].values,
    )
    return pairs


def filter_constraints(constraints, pairs):
    pairs = pairs.loc[
        pairs["origin_distance"].le(constraints["space_constraint"])
        & pairs["destination_distance"].le(constraints["space_constraint"])
    ]

    pairs = pairs.loc[
        pairs["departure_time_difference"].between(
            -constraints["time_constraint"], constraints["time_constraint"]
        )
        | pairs["arrival_time_difference"].between(
            -constraints["time_constraint"], constraints["time_constraint"]
        )
    ]
    return pairs
