<!--
Copyright Inria
Contributor: Aina Rasoldier, (2023)

aina.rasoldier@inria.fr

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
-->

# Carpooling potential analyzer

_Carpooling potential analyzer_ is a software in Python that can compute carpooling scenarios maximizing the avoided VKT from a given travel demand.

## System requirements

The values below are given for a run on a computer with the following material configuration and the default model configuration:
- Processor: Intel® Core™ i5-4590 CPU @ 3.30GHz × 4
- Memory: 16.0 GiB
- OS: Ubuntu 22.04.2 LTS (64-bit)

For running the carpooling potential analyzer on Isère, you need at least
- **15 GB of memory**, and
- **more than 5 GB of free disk space**.

The details are written below.

The total time to set up and run the model is **between 30 min and 1 hour**.

### Environment requirement

If using Micromamba (least disk usage demanding set-up), you will need at least *2.3 GB* for the Anaconda/Micromamba environment (Micromamba is 14 MB, it will download 309 MB and after all the package installations, the folder of Micromamba will be 2.3 GB).

If you also want to have the environment for the population synthesis, you will need at least *4.1 GB* of free space.

### Population Synthesis
For your information, here are the disk requirement for the [population synthesis](https://github.com/eqasim-org/ile-de-france).

- Synthetic population for Île-de-France: 3.1 GB of cache
- Synthetic population for Isère:
    - *477 MB* of cache
    - 11 min of runtime
    - Memory peak of *14.4 GB*

### Carpooling analyzer

### With trips from Île-de-France 
- Maximum memory usage: 7.2 GB
- Runtime: 7 min
- Cache size (disk usage on top of the size of this repo): 588 MB

### With trips from Isère
- Maximum memory usage: 4.5 GB
- Runtime: 2 min 24 s
- Cache size (disk usage on top of the size of this repo): 117M

## Install the environment

To install all the dependencies using Anaconda, Mamba or Micromamba, just run:

```
conda env create -f environment.yml
```

or

```
mamba env create -f environment.yml
```

or

```
micromamba env create -f environment.yml
```

Then, activate the environment:

```
conda activate carpooling
```

or, if using Micromamba:

```
micromamba activate carpooling
```

## Create a configuration file

This model is customizable following the example in *config.example.yml*.

For a first glance, just copy *config.example.yml* to *config.yml* and set `paths.trips` the location of the input travel demand. The input travel demand can be generated thanks to https://github.com/eqasim-org/ile-de-france.


## Launch the model
Then, to launch the Carpooling Potential model, execute this command in the root directory (the same as this README).

```
python -m synpp
```

By default, the configuration file `config.yml` is used. But you can use another configuration file if you want:

```
python -m synpp another_config.yml
```

## Computing itineraries

Itineraries are used for the inclusive carpooling scheme model. You can compute it either by using OSMNx+Pandana or GraphHopper.
Note that changing the routing tool will change the carpoolable trips.
GraphHopper is probably the most reliable routing tool. Indeed, the current use of OSMNx+Pandana for the Carpooling Potential Analyzer is mostly a way to make the software standalone: no verification of the actual realism of the itineraries is made, especially on the timing of the routes.

### Using OSMNx+Pandana
In the configuration file, set `routes.tool` to "pandana".

### Using GraphHopper
In the configuration file, set `routes.tool` to "graphhopper".

To use GraphHopper to compute the itineraries, you need to run an instance of the GraphHopper server.
Please follow the instruction on the [official repository](https://github.com/graphhopper/graphhopper).
For example, here is [the documentation to install GraphHopper 7.0](https://github.com/graphhopper/graphhopper/tree/7.x#installation).
Then, the launch command should be like this:
```
java -Xmx16G -jar path/to/graphhopper.jar server path/to/config.yml
```
