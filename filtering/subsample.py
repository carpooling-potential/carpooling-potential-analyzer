# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
# 
# aina.rasoldier@inria.fr
# 
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Subsample population randomly."""
import pandas as pd
import numpy as np
import synpp

from commons import print_colored, EscapeSequences


def configure(context: synpp.ConfigurationContext):
    context.config("subsampling.seed")
    context.config("subsampling.rate")
    context.stage("synthesis.cleaned")


def execute(context: synpp.ExecuteContext):
    seed: int = context.config("subsampling.seed")
    rate: float = context.config("subsampling.rate")
    trips: pd.DataFrame = context.stage("synthesis.cleaned")

    print_colored(
        EscapeSequences.LIGHTBLUE, f"Initial number of trips: {trips.shape[0]}"
    )

    person_ids = trips["person_id"].unique()
    if seed == "none":
        generator = np.random.default_rng()
    else:
        generator = np.random.default_rng(seed=seed)

    mask = generator.binomial(n=1, p=rate, size=person_ids.shape[0])
    chosen_persons = person_ids[mask == 1]
    chosen_trips = trips["person_id"].isin(chosen_persons)

    print_colored(
        EscapeSequences.LIGHTBLUE, f"Trips in subsample: {chosen_trips.sum()}"
    )

    return chosen_trips
