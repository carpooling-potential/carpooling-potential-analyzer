# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
#
# aina.rasoldier@inria.fr
#
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""
Unary filtering is the first step of the potential analysis.

Here, trips can be directly removed according to configurable criterias on data
properties
(properties on the output of the population synthesis, that are households,
persons and trips).
"""
import synpp
import pandas as pd
from geopandas import GeoSeries

from commons import get_dest, get_orig


def configure(context):
    """Configure."""
    context.stage("synthesis.metadata")
    context.config("carpoolable")

    if context.config("carpoolable.select_flows") is True:
        context.stage("filtering.selected_flows")


def execute(context: synpp.ExecuteContext):
    """Execute."""
    carpoolable_config = context.config("carpoolable")
    trips = context.stage("synthesis.metadata")

    if context.config("carpoolable.select_flows") is True:
        # Select flows
        carpoolable = context.stage("filtering.selected_flows")
    else:
        carpoolable = pd.Series(True, index=trips.index)

    print("Number of initial trips:", trips.shape[0])
    # "Transferred modes" are modes of trips we would carpool.
    modes = carpoolable_config["modes"]
    print("Selecting trips of modes", modes, "(from config)...")
    selected_by_mode = trips["mode"].isin(modes)
    print(selected_by_mode.sum(), "trips selected. Keeping only them...")
    carpoolable &= trips["mode"].isin(modes)

    print("Selecting trips where origin equals destination...")
    eq_od = GeoSeries(get_orig(trips)).geom_equals(GeoSeries(get_dest(trips)))
    print(eq_od.sum(), "trips have origin == destination. Deleting them...")
    carpoolable &= ~eq_od

    print("Selecting trips where distance is strictly positive...")
    positive_distance = trips["routed_distance"].gt(0)
    print(
        positive_distance.sum(),
        "trips have a strictly positive distance. Keeping only them...",
    )
    carpoolable &= positive_distance

    print("Number of carpoolable trips:", carpoolable.sum())

    driver_carpoolable_purposes = carpoolable_config["driver"]
    print(
        "Setting non carpoolable trips...\nDriver carpoolable purposes:",
        driver_carpoolable_purposes,
        "(from config)",
    )

    if driver_carpoolable_purposes == "all":
        driver_carpoolable = carpoolable
    else:
        driver_carpoolable = pd.Series(
            False, index=trips.index, name="driver_carpoolable"
        )
        for (
            preceding_purpose,
            following_purpose,
        ) in driver_carpoolable_purposes:
            driver_carpoolable.loc[
                carpoolable
                & trips["preceding_purpose"].eq(preceding_purpose)
                & trips["following_purpose"].eq(following_purpose)
            ] = True
    print("Number of driver carpoolable trips:", driver_carpoolable.sum())

    passenger_carpoolable_purposes = carpoolable_config["passenger"]
    print(
        "Setting non carpoolable trips...\nPassenger carpoolable purposes:",
        passenger_carpoolable_purposes,
        "(from config)",
    )

    if passenger_carpoolable_purposes == "all":
        passenger_carpoolable = carpoolable
    else:
        passenger_carpoolable = pd.Series(
            False, index=trips.index, name="passenger_carpoolable"
        )
        for (
            preceding_purpose,
            following_purpose,
        ) in passenger_carpoolable_purposes:
            passenger_carpoolable.loc[
                carpoolable
                & trips["preceding_purpose"].eq(preceding_purpose)
                & trips["following_purpose"].eq(following_purpose)
            ] = True
    print(
        "Number of passenger carpoolable trips:", passenger_carpoolable.sum()
    )

    context.set_info(
        "passenger_carpoolable_vmt",
        trips.loc[passenger_carpoolable, "routed_distance"].sum(),
    )

    context.set_info(
        "driver_carpoolable_vmt",
        trips.loc[driver_carpoolable, "routed_distance"].sum(),
    )

    context.set_info(
        "carpoolable_vmt",
        trips.loc[
            driver_carpoolable | passenger_carpoolable, "routed_distance"
        ].sum(),
    )
    return driver_carpoolable, passenger_carpoolable
