# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
# 
# aina.rasoldier@inria.fr
# 
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Get the generated population."""

import os
import geopandas as gpd
from shapely.geometry import Point

from commons import catchtime


def configure(context):
    context.config("paths.trips")


def clean(trip_path: str):
    with catchtime("Reading trips"):
        df_trips = gpd.read_file(trip_path)

    # Useful columns
    with catchtime("Create column for euclidean distance"):
        df_trips["euclidean_distance"] = df_trips.length

    with catchtime("Create column for preceding geometry"):
        df_trips["proj_preceding_geometry"] = gpd.GeoSeries(
            df_trips["geometry"].apply(lambda g: Point(g.coords[0])), crs=2154
        )
        df_trips["preceding_geometry"] = df_trips[
            "proj_preceding_geometry"
        ].to_crs(epsg=4326)

    with catchtime("Create column for following geometry"):
        df_trips["proj_following_geometry"] = gpd.GeoSeries(
            df_trips["geometry"].apply(lambda g: Point(g.coords[1])), crs=2154
        )
        df_trips["following_geometry"] = df_trips[
            "proj_following_geometry"
        ].to_crs(epsg=4326)

    with catchtime("Changing type of waypoint to tuple"):
        df_trips["waypoints"] = df_trips["geometry"].apply(
            lambda line: tuple(line.coords)
        )

    with catchtime("Changing type of times to int"):
        df_trips = df_trips.astype(
            {
                "departure_time": int,
                "arrival_time": int,
            }
        )
    return df_trips


def execute(context):
    return clean(context.config("paths.trips"))


def validate(context):
    path = context.config("paths.trips")
    filesize = os.path.getsize(path)
    return filesize
