# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
# 
# aina.rasoldier@inria.fr
# 
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Query itineraries from GH for synthetic trips."""
from datetime import datetime

import pandas as pd
from synthesis.routed import departure_constrained_mask


def configure(context):
    context.config("paths.trips")
    context.stage("synthesis.cleaned", alias="trips")
    context.stage("synthesis.routes.unique.pt")
    context.stage("synthesis.routes.unique.pt_arrive_by")
    context.config("routes.departure_constrained")


def process_legs(legs):
    if isinstance(legs, list) and len(legs) > 1:
        walk_distance_before = legs[0]["distance"]
        walk_distance_after = legs[-1]["distance"]
        waiting_time = 0
        transport_time = 0
        walk_arrival_time = None
        for leg in legs:
            if walk_arrival_time is None:
                walk_arrival_time = datetime.fromisoformat(
                    leg["departure_time"]
                )
            if leg["type"] == "walk":
                walk_arrival_time = datetime.fromisoformat(leg["arrival_time"])
            elif leg["type"] == "pt":
                entrance_time = datetime.fromisoformat(
                    leg["stops"][0]["departure_time"]
                )
                exit_time = datetime.fromisoformat(
                    leg["stops"][-1]["arrival_time"]
                )
                transport_time += (exit_time - entrance_time).total_seconds()
                waiting_time += (
                    entrance_time - walk_arrival_time
                ).total_seconds()

        return pd.Series(
            (
                walk_distance_before,
                walk_distance_after,
                waiting_time,
                transport_time,
            )
        )
    return pd.Series((None, None, None, None))


def execute(context):
    synthesis = context.stage("trips")
    df_routes = context.stage("synthesis.routes.unique.pt")

    departure_constrained = departure_constrained_mask(
        synthesis, context.config("routes.departure_constrained")
    )

    df_trips_on_departure = pd.merge(
        synthesis.loc[departure_constrained].reset_index(),
        df_routes,
        left_on=["preceding_geometry", "following_geometry", "departure_time"],
        right_on=["preceding_geometry", "following_geometry", "time"],
        how="left",
    ).set_index("index")
    df_trips_on_departure["arrival_time"] = (
        df_trips_on_departure["departure_time"]
        + df_trips_on_departure["duration_computed"]
    )

    df_routes_arrive_by = context.stage("synthesis.routes.unique.pt_arrive_by")
    df_trips_on_arrival = pd.merge(
        synthesis.loc[~departure_constrained].reset_index(),
        df_routes_arrive_by,
        left_on=["preceding_geometry", "following_geometry", "arrival_time"],
        right_on=["preceding_geometry", "following_geometry", "time"],
        how="left",
    ).set_index("index")
    df_trips_on_departure["departure_time"] = (
        df_trips_on_departure["arrival_time"]
        - df_trips_on_departure["duration_computed"]
    )

    df_trips = pd.concat([df_trips_on_departure, df_trips_on_arrival])

    # with catchtime("Post-processing public transport routes"):
    #     df_trips[
    #         [
    #             "walk_distance_before",
    #             "walk_distance_after",
    #             "waiting_time",
    #             "transport_time",
    #         ]
    #     ] = df_trips["legs"].apply(process_legs)

    return df_trips
