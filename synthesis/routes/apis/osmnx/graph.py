# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
# 
# aina.rasoldier@inria.fr
# 
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Create NetworkX graph from OSM data."""
import synpp
import geopandas as gpd
import osmnx.speed
import osmnx.distance
import osmnx.graph
import shapely
import pickle

from commons import catchtime, get_geometries


def configure(context: synpp.ConfigurationContext):
    if not context.config("paths.oxgraph"):
        context.stage("synthesis.cleaned", alias="trips")


def _get_bounding_polygon(geometries: gpd.GeoSeries) -> shapely.Polygon:
    with catchtime("Getting bounding polygon"):
        return geometries.unary_union.convex_hull.buffer(0.01)


def _get_graph(bounding_polygon: shapely.Polygon):
    with catchtime("Getting graph..."):
        oxgraph = osmnx.graph.graph_from_polygon(
            bounding_polygon,
            network_type="drive",
            truncate_by_edge=True,
            retain_all=True,
            clean_periphery=False
        )
    with catchtime("Adding edge attributes..."):
        oxgraph = osmnx.speed.add_edge_speeds(oxgraph)
        oxgraph = osmnx.distance.add_edge_lengths(oxgraph)
        oxgraph = osmnx.speed.add_edge_travel_times(
            oxgraph, precision=3
        )
    return oxgraph


def execute(context: synpp.ExecuteContext):
    if isinstance(context.config("paths.oxgraph"), str):
        with open(context.config("paths.oxgraph"), "rb") as f:
            return pickle.load(f)

    trips = context.stage("trips")
    pts = get_geometries(trips)
    bounding_polygon = _get_bounding_polygon(pts)
    return _get_graph(bounding_polygon)
