# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
# 
# aina.rasoldier@inria.fr
# 
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Wrapper around OSMNx to calculate car itineraries."""
import numpy as np
import osmnx.graph
import osmnx.distance
import osmnx.folium
import osmnx.utils_graph
import osmnx.speed
import geopandas as gpd
import pandas as pd
import shapely
import networkx as nx

from commons import get_unique_trips, catchtime


def graph_route_to_linestring(graph: nx.MultiDiGraph, route: list):
    """
    Transform path in OSMnx graph to LineString.

    Taken from source code of OSMnx.
    """
    # assemble the route edge geometries' x and y coords then plot the line
    x = []
    y = []
    for u, v in zip(route[:-1], route[1:]):
        # if there are parallel edges, select the shortest in length
        data = min(
            graph.get_edge_data(u, v).values(), key=lambda d: d["length"]
        )
        if "geometry" in data:
            # if geometry attribute exists, add all its coords to list
            xs, ys = data["geometry"].xy
            x.extend(xs)
            y.extend(ys)
        else:
            # otherwise, the edge is a straight line from node to node
            x.extend((graph.nodes[u]["x"], graph.nodes[v]["x"]))
            y.extend((graph.nodes[u]["y"], graph.nodes[v]["y"]))
    ls = shapely.LineString(zip(x, y)).simplify(0)
    return ls


def process_path(context, path):
    graph = context.data("graph")
    if path is None or len(path) <= 1:
        timing = np.zeros((1, 2))
        route = shapely.LineString([])
    else:
        timing_distance = np.array(
            osmnx.utils_graph.get_route_edge_attributes(
                graph, path, attribute="length"
            )
        )
        timing_time = np.array(
            osmnx.utils_graph.get_route_edge_attributes(
                graph, path, attribute="travel_time"
            )
        )
        timing = np.stack((timing_distance, timing_time)).T.cumsum(axis=0)
        timing = np.vstack(((0, 0), timing))
        route = graph_route_to_linestring(graph, path)

    context.progress.update()
    return timing, route


def process_paths(progress, parallel, graph: nx.MultiGraph, paths: list):
    with progress(total=len(paths), label="Processing paths"):
        with parallel(data={"graph": graph}) as p:
            timings, routes = zip(*p.imap(process_path, paths))

    distances, durations = zip(*(t[-1, :] for t in timings))

    return routes, distances, durations, timings


def get_nearest_nodes(oxgraph, origs, dests):
    pts = gpd.GeoSeries(pd.concat((origs, dests)))
    with catchtime("Getting nearest nodes"):
        nearest_nodes = osmnx.distance.nearest_nodes(oxgraph, pts.x, pts.y)
    orig_node_ids = nearest_nodes[: origs.shape[0]]
    dest_node_ids = nearest_nodes[origs.shape[0] :]
    return orig_node_ids, dest_node_ids


def _get_shortest_paths(
    oxgraph: nx.MultiDiGraph, orig_node_ids, dest_node_ids
):
    with catchtime("Getting shortest paths"):
        shortest_paths = osmnx.distance.shortest_path(
            oxgraph,
            orig_node_ids,
            dest_node_ids,
            weight="travel_time",
            cpus=None,
        )

    return shortest_paths


def get_routes_wrapper(
    progress,
    parallel,
    oxgraph: nx.MultiDiGraph,
    origs,
    dests,
):
    trips = gpd.GeoDataFrame(
        {
            "preceding_geometry": gpd.GeoSeries(list(origs)),
            "following_geometry": gpd.GeoSeries(list(dests)),
        }
    )

    trips = get_unique_trips(trips)
    orig_node_ids, dest_node_ids = get_nearest_nodes(
        oxgraph, trips["preceding_geometry"], trips["following_geometry"]
    )
    shortest_paths = _get_shortest_paths(
        oxgraph, orig_node_ids, dest_node_ids
    )

    routes, distances, durations, timings = process_paths(
        progress, parallel, oxgraph, shortest_paths
    )

    gdf_routes = gpd.GeoDataFrame(
        {
            "routed_distance": distances,
            "duration_computed": durations,
            "timing": timings,
            "route": routes,
            "preceding_geometry": trips["preceding_geometry"],
            "following_geometry": trips["following_geometry"],
        },
        geometry="route",
    )

    return gdf_routes
