# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
# 
# aina.rasoldier@inria.fr
# 
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Utilitaries to query itineraries from GraphHopper."""

from pytz import timezone
from datetime import datetime, timedelta, UTC
from typing import Iterable

import pandas as pd
import geopandas as gpd
from shapely.geometry import LineString
from numpy import array
import aiohttp
import asyncio
from dataclasses import dataclass
import typing as tp
import queue

from commons import (
    catchtime,
    print_colored,
    EscapeSequences,
    get_unique_trips,
    DATE,
)


HOST = "localhost"


@dataclass
class TaskPool:
    semaphore: tp.Optional[asyncio.Semaphore]
    tasks: tp.Set[asyncio.Task]
    results: tp.List[tp.Any]
    closed: bool

    def __init__(self, workers: int):
        self.semaphore = asyncio.Semaphore(workers) if workers else None
        self.tasks = set()
        self.results = list()
        self.closed = False
        self.result_queue = queue.Queue()
        self.workers = workers

    async def put(self, coro_f: tp.Callable[[], tp.Awaitable]):
        if self.closed:
            raise RuntimeError("Trying put items into a closed TaskPool")

        if self.semaphore is not None:
            await self.semaphore.acquire()

        task_index = len(self.tasks)
        task = asyncio.ensure_future(self._get_task(coro_f, task_index))
        task.add_done_callback(self._on_task_done)

        self.tasks.add(task)

    async def _get_task(self, coro: tp.Callable[[], tp.Awaitable], task_idx):
        result = await coro
        self.result_queue.put((task_idx, result))

    def _on_task_done(self, task):
        if self.semaphore is not None:
            self.semaphore.release()

    async def _join(self):
        try:
            await asyncio.gather(*self.tasks)
        finally:
            self.closed = True

            dict_results = dict()
            while not self.result_queue.empty():
                task_idx, result = self.result_queue.get()
                dict_results[task_idx] = result

            for i in range(len(dict_results)):
                self.results.append(dict_results[i])

    async def __aenter__(self):
        return self

    async def __aexit__(self, exc_type, exc, tb):
        await self._join()

    def __len__(self) -> int:
        return len(self.tasks)


def _to_iso_datetime(seconds: int):
    date = datetime(*DATE, tzinfo=timezone("CET")) + timedelta(seconds=seconds)
    return date.astimezone(UTC).isoformat(timespec="seconds")


def _build_urls(
    orig_x,
    orig_y,
    dest_x,
    dest_y,
    timestamps,
    port: int,
    mode: str,
    get_details: bool,
    calc_points: bool,
    arrive_by: bool,
):
    orig_x_s = pd.Series(list(orig_x), dtype=str)
    orig_y_s = pd.Series(list(orig_y), dtype=str)
    dest_x_s = pd.Series(list(dest_x), dtype=str)
    dest_y_s = pd.Series(list(dest_y), dtype=str)

    urls = (
        "http://"
        + HOST
        + ":"
        + str(port)
        + "/route?instructions=false&points_encoded=false"
        + "&alternative_route.max_paths=1&profile="
        + mode
        + "&point="
        + orig_y_s
        + ","
        + orig_x_s
        + "&point="
        + dest_y_s
        + ","
        + dest_x_s
    )

    if get_details:
        urls += "&details=time&details=distance"

    if timestamps is not None:
        timestamps_s = pd.Series(list(timestamps), dtype=str)
        urls += "&pt.earliest_departure_time=" + timestamps_s

    if arrive_by:
        urls += "&pt.arrive_by=true"

    if not calc_points:
        urls += "&calc_points=false"

    return urls


async def _fetch(progress, url: str, session: aiohttp.ClientSession):
    while True:
        try:
            async with session.get(url) as response:
                response = await response.json()
                break
        except (
            aiohttp.ServerDisconnectedError,
            aiohttp.ClientResponseError,
            aiohttp.ClientConnectorError,
        ) as e:
            print(f"Oops, the server connection was dropped on {url}: {e}")
            await asyncio.sleep(1)

    progress.update()
    return response


async def _query_urls(progress_fun, urls):
    connector = aiohttp.TCPConnector(limit=None, force_close=True)
    with progress_fun(
        urls, label="Get itineraries", total=len(urls)
    ) as progress:
        async with aiohttp.ClientSession(connector=connector) as session:
            async with TaskPool(500) as tasks:
                for url in urls:
                    await tasks.put(_fetch(progress, url, session))
    assert len(tasks.results) == len(urls)
    return tasks.results


def _create_timing(details):
    """Create a 2D list where col 0 is distance and col 1 is time."""
    timing = [(0, 0)]
    current_distance = 0
    current_time = 0
    for distance, time in zip(details["distance"], details["time"]):
        current_distance += distance[2]
        current_time += time[2]
        timing.append((current_distance, current_time / 1000))
    return timing


def _process_responses(responses: list):
    responses = list(
        map(lambda s: s if isinstance(s, dict) else dict(), responses)
    )
    paths_s = pd.DataFrame(responses)["paths"]
    paths_notna = paths_s.loc[paths_s.notna()]
    df = pd.DataFrame.from_records(
        pd.DataFrame(paths_notna.tolist())[0].tolist(),
        index=paths_notna.index,
        columns=("points", "details", "time", "distance", "transfers"),
    )
    try:
        df["route"] = gpd.GeoSeries(
            pd.DataFrame(df["points"].tolist())["coordinates"].apply(
                LineString
            )
        )
    except KeyError:
        pass

    try:
        df["timing"] = df["details"].apply(_create_timing).apply(array)
    except (KeyError, TypeError):
        pass

    df["duration_computed"] = df["time"] / 1000
    df = (
        df.rename(columns={"distance": "routed_distance"})
        .drop(columns=["time", "details", "points"])
        .reindex(index=paths_s.index, fill_value=None)
    )
    if "route" in df:
        return gpd.GeoDataFrame(df, geometry="route")
    return df


def get_routes_wrapper(
    progress,
    origs: Iterable,
    dests: Iterable,
    times: "Iterable|None",
    mode: str,
    get_details: bool,
    get_points: bool,
    arrive_by: bool,
):
    """Get routes from GraphHopper."""
    trips = gpd.GeoDataFrame(
        {
            "preceding_geometry": gpd.GeoSeries(list(origs)),
            "following_geometry": gpd.GeoSeries(list(dests)),
        }
    )

    if times is not None:
        with catchtime("Creating timestamp column"):
            trips["time"] = pd.Series(list(times), dtype=int)
            iso_time = (
                trips["time"]
                .apply(_to_iso_datetime)
                .str.replace("+00:00", "Z", regex=False)
            )

    trips = get_unique_trips(trips)

    with catchtime("Building urls"):
        urls = _build_urls(
            trips["preceding_geometry"].x,
            trips["preceding_geometry"].y,
            trips["following_geometry"].x,
            trips["following_geometry"].y,
            None if times is None else iso_time,
            8989,
            mode,
            get_details,
            get_points,
            arrive_by,
        )

    responses = asyncio.run(_query_urls(progress, urls))

    with catchtime("Process responses"):
        routes = _process_responses(responses)

    print_colored(
        EscapeSequences.LIGHTBLUE, f"Routes found: {len(routes)}/{len(trips)}."
    )

    # Completing newly created route GeoDataFrame with initial data.
    routes["preceding_geometry"] = trips["preceding_geometry"]
    routes["following_geometry"] = trips["following_geometry"]

    if times is not None:
        routes["time"] = trips["time"]

    return routes.set_crs(epsg=4326)
