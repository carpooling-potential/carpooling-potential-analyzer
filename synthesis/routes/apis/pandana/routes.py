# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
#
# aina.rasoldier@inria.fr
#
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Wrapper around OSMNx to calculate car itineraries."""
import geopandas as gpd
import networkx as nx
from pandana.network import Network
import osmnx.utils_graph

from commons import get_unique_trips, catchtime
from synthesis.routes.apis.osmnx.routes import (
    process_paths,
    get_nearest_nodes,
)


def _get_shortest_paths(pandana_net: Network, orig_node_ids, dest_node_ids):
    with catchtime("Getting shortest paths (Pandana)"):
        shortest_paths = pandana_net.shortest_paths(
            orig_node_ids,
            dest_node_ids,
            imp_name="travel_time",
        )

    return shortest_paths


def nx_to_pandana(nx_graph):
    gdf_nodes, gdf_edges = osmnx.utils_graph.graph_to_gdfs(nx_graph)
    edges = gdf_edges.reset_index()
    with catchtime("Preparing Pandana network"):
        return Network(
            gdf_nodes["x"],
            gdf_nodes["y"],
            edges["u"],
            edges["v"],
            edges[["travel_time"]],
            twoway=False,
        )


def get_routes_wrapper(
    progress,
    parallel,
    oxgraph: nx.MultiDiGraph,
    origs,
    dests,
):
    trips = gpd.GeoDataFrame(
        {
            "preceding_geometry": gpd.GeoSeries(list(origs)),
            "following_geometry": gpd.GeoSeries(list(dests)),
        }
    )

    trips = get_unique_trips(trips)
    orig_node_ids, dest_node_ids = get_nearest_nodes(
        oxgraph, trips["preceding_geometry"], trips["following_geometry"]
    )

    pandana_net = nx_to_pandana(oxgraph)
    shortest_paths = _get_shortest_paths(
        pandana_net, orig_node_ids, dest_node_ids
    )

    routes, distances, durations, timings = process_paths(
        progress, parallel, oxgraph, shortest_paths
    )

    gdf_routes = gpd.GeoDataFrame(
        {
            "routed_distance": distances,
            "duration_computed": durations,
            "timing": timings,
            "route": routes,
            "preceding_geometry": trips["preceding_geometry"],
            "following_geometry": trips["following_geometry"],
        },
        geometry="route",
    ).set_crs(epsg=4326)

    return gdf_routes
