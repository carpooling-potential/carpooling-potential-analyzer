# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
# 
# aina.rasoldier@inria.fr
# 
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Request car itineraries to GraphHopper."""
import synpp

from synthesis.routes.apis.osmnx.routes import (
    get_routes_wrapper as get_routes_wrapper_osmnx,
)
from synthesis.routes.apis.pandana.routes import (
    get_routes_wrapper as get_routes_wrapper_pandana,
)
from synthesis.routes.apis.graphhopper.routes import (
    get_routes_wrapper as get_routes_wrapper_graphhopper,
)


def configure(context: synpp.ConfigurationContext):
    context.stage("synthesis.geofiltered")
    if context.config("routes.tool") in ("pandana", "osmnx"):
        context.stage("synthesis.routes.apis.osmnx.graph", alias="graph")


def execute(context: synpp.ExecuteContext):
    trips = context.stage("synthesis.geofiltered")
    tool_choice = context.config("routes.tool")

    if tool_choice in ("pandana", "osmnx"):
        oxgraph = context.stage("graph")

    match tool_choice:
        case "pandana":
            routes = get_routes_wrapper_pandana(
                context.progress,
                context.parallel,
                oxgraph,
                trips["preceding_geometry"],
                trips["following_geometry"],
            )
        case "osmnx":
            routes = get_routes_wrapper_osmnx(
                context.progress,
                context.parallel,
                oxgraph,
                trips["preceding_geometry"],
                trips["following_geometry"],
            )
        case "graphhopper":
            routes = get_routes_wrapper_graphhopper(
                context.progress,
                trips["preceding_geometry"],
                trips["following_geometry"],
                times=None,
                mode="car",
                get_details=True,
                get_points=True,
                arrive_by=False,
            )

    routes["mode"] = "car"
    return routes
