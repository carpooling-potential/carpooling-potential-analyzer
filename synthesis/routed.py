# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
#
# aina.rasoldier@inria.fr
#
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Associate car route with trips."""

import warnings


import pandas as pd
import geopandas as gpd

warnings.filterwarnings("ignore", "GeoSeries.notna", UserWarning)


def configure(context):
    context.config("paths.trips")
    context.config("routes.departure_constrained")
    context.stage("synthesis.geofiltered")
    context.stage("synthesis.routes.unique.car", alias="routes")


def departure_constrained_mask(trips, purposes):
    on_departure = pd.Series(False, index=trips.index)
    for preceding_purpose, following_purpose in purposes:
        on_departure |= trips["preceding_purpose"].eq(
            preceding_purpose
        ) & trips["following_purpose"].eq(following_purpose)
    return on_departure


def correct_time(trips: gpd.GeoDataFrame) -> gpd.GeoDataFrame:
    trips.loc[
        trips["duration_computed"].notna() & trips["departure_constrained"],
        "arrival_time",
    ] = (
        trips["departure_time"] + trips["duration_computed"]
    )
    trips.loc[
        trips["duration_computed"].notna() & ~trips["departure_constrained"],
        "departure_time",
    ] = (
        trips["arrival_time"] - trips["duration_computed"]
    )
    return trips


def execute(context):
    trips: pd.DataFrame = context.stage("synthesis.geofiltered")
    routes = context.stage("routes").drop(columns="mode")

    assert (
        ~routes["route"].is_empty & routes["route"].notna()
    ).sum(), "No route found."

    trips = pd.merge(
        trips.reset_index(),
        routes,
        left_on=["preceding_geometry", "following_geometry"],
        right_on=["preceding_geometry", "following_geometry"],
        how="left",
    ).set_index("index")

    trips.loc[
        trips["mode"].ne("car"),
        ["timing", "route", "duration_computed"],
    ] = None

    assert (
        ~trips["route"].is_empty & trips["route"].notna()
    ).sum(), "No corresponding route found."

    purposes = context.config("routes.departure_constrained")

    print(
        "Setting trips which will be constrained on departure:",
        purposes,
        "(from config)",
    )

    trips["departure_constrained"] = departure_constrained_mask(
        trips, purposes
    )
    print(
        "Number of trips with constraint on departure:",
        trips["departure_constrained"].sum(),
    )

    return correct_time(gpd.GeoDataFrame(trips, geometry="route"))
