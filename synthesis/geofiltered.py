# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
#
# aina.rasoldier@inria.fr
#
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Keep trips of Grande Région Grenobloise."""
import os
import pandas as pd
import geopandas as gpd
from py7zr import SevenZipFile

from synpp import ExecuteContext, ConfigurationContext
from commons import catchtime


def configure(context: ConfigurationContext):
    context.config("paths.iris")
    context.config("synthesis.communes")
    context.stage("synthesis.cleaned", alias="trips")


def all_od_in_perimeter(trips: gpd.GeoDataFrame) -> "pd.Series[bool]":
    """Select trips of persons whose all trip O and D are in the perimeter."""
    # Select persons with at least one trip with a trip outside the zone.
    selected_persons = trips.loc[
        ~trips["od_in_perimeter"], "person_id"
    ].unique()
    return ~trips["person_id"].isin(selected_persons)


def od_in_perimeter(trips: gpd.GeoDataFrame, zones: gpd.GeoSeries, progress):
    """Select trips with origin and destination in zone."""
    geoms = gpd.GeoSeries(
        pd.concat(
            (
                trips["proj_preceding_geometry"],
                trips["proj_following_geometry"],
            )
        )
    )
    mask = pd.Series(False, index=geoms.index, dtype=bool)
    for _idx, zone in progress(
        zones.items(),
        total=zones.shape[0],
        label="Calculate geometry inclusions",
    ):
        mask |= geoms.intersects(zone)

    return mask.values[: trips.shape[0]] & mask.values[trips.shape[0] :]


def get_geomask(
    trips: gpd.GeoDataFrame, zones: gpd.GeoSeries, progress
) -> "pd.Series[bool]":
    print("Number of trips (before)", trips.shape[0])
    print("Number of persons (before)", len(trips["person_id"].unique()))

    trips["od_in_perimeter"] = od_in_perimeter(trips, zones, progress)

    # Delete all persons with a trip where origin or destination is outside the
    # zone, like in population synthesis
    mask = all_od_in_perimeter(trips)
    print("Number of trips (after)", mask.sum())
    print(
        "Number of persons (after)",
        len(trips.loc[mask, "person_id"].unique()),
    )
    return mask


def filter_parts(zones: gpd.GeoDataFrame, communes: list) -> gpd.GeoDataFrame:
    return zones.loc[zones["INSEE_COM"].isin(communes)]


def execute(context: ExecuteContext):
    req_communes = list(map(str, context.config("synthesis.communes")))
    path_iris = context.config("paths.iris")
    trips: gpd.GeoDataFrame = context.stage("trips")

    if len(req_communes) == 0 or path_iris is False:
        print("No requested commune for filtering or no path to IRIS.")
        print("Ignoring geofiltering.")
        return trips

    with SevenZipFile(path_iris) as ar, catchtime("Extracting IRIS"):
        paths = [p for p in ar.getnames() if "LAMB93" in p]
        ar.extract(context.cache_path, paths)

    iris_df: gpd.GeoDataFrame = gpd.read_file(
        os.path.join(
            context.cache_path,
            next(filter(lambda s: s.endswith(".shp"), paths)),
        )
    )

    perimeter_parts = filter_parts(iris_df, req_communes).make_valid()
    geofilter = get_geomask(trips, perimeter_parts, context.progress)
    assert geofilter.sum(), "Nobody is in the geofilter."

    geofiltered = trips.loc[geofilter]
    return geofiltered
