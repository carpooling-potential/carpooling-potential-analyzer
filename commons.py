# Copyright Inria
# Contributor: Aina Rasoldier, (2023)
# 
# aina.rasoldier@inria.fr
# 
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
# 
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
# 
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
# 
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
# 
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

"""Gather common utility functions."""

import json
import os
import psutil
import datetime
import itertools

from time import perf_counter, time
from enum import Enum
from typing import Iterable

import yaml
from yaml.loader import SafeLoader
import networkx as nx
import geopandas as gpd
import pandas as pd

import synpp


DATE = [2022, 12, 5]


class EscapeSequences(str, Enum):
    """Color text printing."""

    LIGHTRED = "\033[91m"
    LIGHTGREEN = "\033[92m"
    LIGHTYELLOW = "\033[93m"
    LIGHTBLUE = "\033[94m"
    LIGHTMAGENTA = "\033[95m"
    LIGHTCYAN = "\033[96m"
    ENDC = "\033[0m"
    BOLD = "\033[1m"
    DIM = "\033[2m"
    UNDERLINE = "\033[4m"
    BLINK = "\033[5m"


def print_colored(sequence: EscapeSequences, text: str):
    """When everything's alright."""
    print(sequence + text + EscapeSequences.ENDC)


def get_shareable_trips_idx(shareability_graph: nx.Graph):
    """Get indexes of trips that have at least a neighbor in the graph."""
    subgraph = nx.subgraph_view(
        shareability_graph,
        filter_node=lambda n: shareability_graph.degree(n) > 0,
    )
    shareable_trips_idx = list(subgraph.nodes)
    return set(shareable_trips_idx)


def get_orig(trip):
    """Get the origin point of trip."""
    return trip["preceding_geometry"]


def get_dest(trip):
    """Get the destination point of trip."""
    return trip["following_geometry"]


class catchtime:
    """Measure execution time of bloc."""

    def __init__(self, name):
        """Initialize the function. Set to give the name."""
        self.name = name

    def __enter__(self):
        """Enter."""
        self.time = perf_counter()
        print(
            "Started",
            EscapeSequences.LIGHTMAGENTA + self.name + EscapeSequences.ENDC,
        )
        return self

    def __exit__(self, type, value, traceback):
        """Exit."""
        self.time = perf_counter() - self.time
        print(
            EscapeSequences.LIGHTMAGENTA + self.name + EscapeSequences.ENDC,
            "done in",
            EscapeSequences.LIGHTCYAN + str(round(self.time, 4)),
            "s" + EscapeSequences.ENDC,
        )


class catchtime_log:
    """Measure execution time of bloc."""

    def __init__(self, name, log):
        """Initialize the function. Set to give the name."""
        self.name = name
        self.log = log

    def __enter__(self):
        """Enter."""
        self.time = perf_counter()
        return self

    def __exit__(self, type, value, traceback):
        """Exit."""
        self.time = perf_counter() - self.time
        self.log.append((time(), self.name, self.time, os.getpid()))


def write_log(logs, filename):
    with open(filename, "w") as f:
        for record in logs:
            f.write(",".join(map(str, record)) + "\n")


def stars_to_groups(stars: nx.DiGraph):
    groups = []
    for unordered_group in nx.connected_components(stars.to_undirected()):
        if len(unordered_group) > 1:
            ordered_group = tuple(
                sorted(
                    unordered_group,
                    key=lambda n: stars.in_degree(n),
                    reverse=True,
                )
            )
            try:
                assert stars.in_degree(ordered_group[1]) == 0
            except AssertionError as err:
                print(unordered_group)
                raise err

            groups.append(ordered_group)
    return groups


def export_to_cyjs(graph, output_path):
    with catchtime("Export graph to Cytoscape JS..."):
        cytoscape_graph = nx.relabel_nodes(
            graph, dict(zip(graph.nodes, map(str, graph.nodes)))
        )
        cytoscape_data = nx.cytoscape_data(cytoscape_graph)

        with open(output_path, "w") as f:
            json.dump(cytoscape_data, f)


def get_run_output_path(context: synpp.ExecuteContext):
    output_path = context.config("paths.output")
    process = psutil.Process(os.getpid())
    start_time = datetime.datetime.fromtimestamp(
        process.create_time()
    ).isoformat(timespec="seconds")
    run_directory_name = str(start_time)
    run_output_path = os.path.join(output_path, run_directory_name)
    config_path = os.path.join(run_output_path, "config.json")

    if not os.path.isdir(run_output_path):
        print_colored(EscapeSequences.LIGHTBLUE, f"Creating {run_output_path}")
        os.makedirs(run_output_path)

    if os.path.isfile(config_path):
        with open(config_path, "r") as f:
            current_config = json.load(f)

    if not os.path.isfile(config_path) or len(current_config) < len(
        context.required_config
    ):
        with open(os.path.join(run_output_path, "config.json"), "w") as f:
            json.dump(context.required_config, f, indent=4)

    return run_output_path


def time_difference(t2: int, t1: int):
    """Operate t2-t1 (in seconds in a day) by handling end and start of day."""
    return ((t2 - t1) + 43200) % 86400 - 43200


def get_unique_trips(trips: gpd.GeoDataFrame) -> gpd.GeoDataFrame:
    print_colored(
        EscapeSequences.LIGHTBLUE, f"Number of trips: {trips.shape[0]}"
    )

    with catchtime("Keep unique O/D"):
        trips = trips.drop_duplicates()
    print("Unique O/D:", trips.shape[0])

    with catchtime("Keep O != D"):
        trips = trips.loc[
            trips["preceding_geometry"].ne(trips["following_geometry"])
        ]
    print_colored(EscapeSequences.LIGHTBLUE, f"O!=D: {trips.shape[0]}")

    return trips.reset_index(drop=True)


def get_geometries(trips):
    return gpd.GeoSeries(
        pd.concat((trips["preceding_geometry"], trips["following_geometry"]))
    )
